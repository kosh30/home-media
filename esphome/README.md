# Install

docker pull esphome/esphome

docker run --rm -v "${PWD}":/config --device=/dev/ttyUSB0 -it esphome/esphome run livingroom.yaml

Note: For ESP32, press boot just before selecting USB0 to enter in Download mode. Can be released as soon as esphome starts writing
