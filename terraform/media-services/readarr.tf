###########################################################################
################################## READARR ################################
###########################################################################
#resource "helm_release" "readarr" {
#  name       = "readarr"
#  chart      = "readarr"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "300"
#  version    = var.versions.readarr.chart
#
#  values = [<<EOF
#image:
#  repository: linuxserver/readarr
#  tag: ${var.versions.readarr.image}
#resources:
#  limits:
#    cpu: 500m
#    memory: 900Mi
#  requests:
#    cpu: 500m
#    memory: 400Mi
#ingress:
#  main:
#    enabled: true
#    hosts:
#      - host: readarr.${data.terraform_remote_state.infra.outputs.local_domain}
#        paths:
#          - path: /
#persistence:
#  config:
#    enabled: true
#    storageClass: ${data.terraform_remote_state.infra.outputs.longhorn_storage_class_name}
#    accessMode: ReadWriteOnce
#    size: 300Mi
#  media:
#    enabled: true
#    emptyDir: false
#    mountPath: /downloads
#    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
#    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
#    accessMode: ReadWriteMany
#EOF
#  ]
#}
#
