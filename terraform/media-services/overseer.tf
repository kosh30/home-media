##########################################################################
################################## OVERSEER ##############################
##########################################################################

resource "kubernetes_persistent_volume" "overseerr_configs" {
  metadata {
    name = "overseerr-configs"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "10Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.overseerr.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim" "overseerr_configs" {
  metadata {
    name = "overseerr-configs"
  }
  spec {
    volume_name        = kubernetes_persistent_volume.overseerr_configs.metadata.0.name
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "10Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_config_map" "overseer_config" {
  metadata {
    name = "overseer-config"
  }

  data = {
    "settings.json" = "${local.overseer_config}"
  }
}

resource "helm_release" "overseer" {
  name       = "overseerr"
  chart      = "overseerr"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "720"
  version    = var.versions.overseerr.chart

  values = [<<EOF
initContainers:
  copy-configmap:
    name: copy-configmap
    image: busybox
    command:
    - "sh"
    - "-c"
    - |
      echo "Copying overseer config file..."
      mkdir -p /opt/app/config
      ls -lah /
      ls -lah /opt/app
      ls -lah /tmp/config
      cp /tmp/config/settings.json /opt/app/settings.json
      echo "--> Current config:"
      cat /opt/app/settings.json
      chmod -R 777 /opt/app
    volumeMounts:
    - name: configfile
      mountPath: /tmp/config
    - name: config
      mountPath: /opt/app
    securityContext:
      runAsUser: 0
image:
  tag: ${var.versions.overseerr.image}
resources:
  limits:
    cpu: 400m
    memory: 350Mi
  requests:
    cpu: 300m
    memory: 300Mi
ingress:
  main:
    enabled: false
    #hosts:
    #  - host: overseer.${data.terraform_remote_state.infra.outputs.local_domain}
    #    paths:
    #      - path: /
persistence:
  configfile:
    enabled: true
    type: custom
    mountPath: /tmp/config
    volumeSpec:
      configMap:
        name: ${kubernetes_config_map.overseer_config.metadata.0.name}
        defaultMode: 0555
  #config:
  #  enabled: true
  #  type: emptyDir
  #  medium: Memory
  #  sizeLimit: 80Mi
  config:
    enabled: true
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${kubernetes_persistent_volume_claim.overseerr_configs.metadata.0.name}
    accessMode: ReadWriteOnce
    size: 10Gi
EOF
  ]
  #depends_on = [helm_release.sonarr]
}

resource "kubernetes_ingress_v1" "overseerr_home" {
  metadata {
    name = "overseerr-home"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      host = "overseerr.${data.terraform_remote_state.infra.outputs.local_domain}"
      http {
        path {
          backend {
            service {
              name = "overseerr"
              port {
                number = 5055
              }
            }
          }
          path = "/"
        }
      }
    }
  }
  depends_on = [helm_release.overseer]
}

#resource "kubernetes_ingress_v1" "overseerr_external" {
#  metadata {
#    name = "overseerr-external"
#    annotations = {
#      "kubernetes.io/ingress.class"    = "nginx"
#      "cert-manager.io/cluster-issuer" = data.terraform_remote_state.infra.outputs.certmanager_clusterissuer_name
#      "nginx.org/redirect-to-https"    = true
#      #"nginx.org/websocket-services" = "homeassistant-home-assistant"
#    }
#  }
#  spec {
#    tls {
#      secret_name = data.terraform_remote_state.infra.outputs.wildcard_ssl_certificate_secret_name
#      hosts       = ["overseerr.${data.terraform_remote_state.infra.outputs.public_domain}"]
#    }
#    rule {
#      host = "overseerr.${data.terraform_remote_state.infra.outputs.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "overseerr"
#              port {
#                number = 5055
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.overseer]
#}
