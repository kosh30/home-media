#resource "kubernetes_deployment_v1" "fileflows" {
#  metadata {
#    name = "fileflows"
#    labels = {
#      app = "fileflows"
#    }
#  }
#  spec {
#    replicas = 1
#    selector {
#      match_labels = {
#        app = "fileflows"
#      }
#    }
#    template {
#      metadata {
#        labels = {
#          app = "fileflows"
#        }
#      }
#      spec {
#        container {
#          image = "revenz/fileflows:23.10"
#          name  = "fileflows"
#
#          env {
#            name  = "TempPathHost"
#            value = "/media/tdarr"
#          }
#
#          resources {
#            limits = {
#              cpu    = "1000m"
#              memory = "900Mi"
#            }
#            requests = {
#              cpu    = "100m"
#              memory = "150Mi"
#            }
#          }
#          liveness_probe {
#            http_get {
#              path = "/"
#              port = 5000
#            }
#          }
#          readiness_probe {
#            http_get {
#              path = "/"
#              port = 5000
#            }
#          }
#          volume_mount {
#            mount_path = "/media"
#            name       = "media"
#          }
#        }
#        volume {
#          name = "media"
#          persistent_volume_claim {
#            claim_name = "multimedia"
#          }
#        }
#      }
#    }
#  }
#  timeouts {
#    create = "1m"
#    delete = "1m"
#    update = "1m"
#  }
#}
#
#resource "kubernetes_service" "fileflows" {
#  metadata {
#    name = "fileflows"
#  }
#  spec {
#    selector = {
#      app = "fileflows"
#    }
#    port {
#      port        = 5000
#      target_port = 5000
#    }
#  }
#}
#
