#########################################################################
################################ PROWLARR ###############################
#########################################################################
resource "kubernetes_persistent_volume_v1" "prowlarr_configs" {
  metadata {
    name = "prowlarr-configs"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "2Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.prowlarr.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim_v1" "prowlarr_configs" {
  metadata {
    name = "prowlarr-config"
  }
  spec {
    volume_name        = kubernetes_persistent_volume_v1.prowlarr_configs.metadata.0.name
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "2Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "helm_release" "prowlarr" {
  name       = "prowlarr"
  chart      = "prowlarr"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "300"
  version    = var.versions.prowlarr.chart

  values = [<<EOF
image:
  repository: linuxserver/prowlarr
  tag: ${var.versions.prowlarr.image}
resources:
  limits:
    cpu: 650m
    memory: 250Mi
  requests:
    cpu: 500m
    memory: 200Mi
ingress:
  main:
    enabled: true
    annotations: 
      kubernetes.io/ingress.class: nginx
    hosts:
      - host: prowlarr.${data.terraform_remote_state.infra.outputs.local_domain}
        paths:
          - path: /
persistence:
  config:
    enabled: true
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${kubernetes_persistent_volume_claim_v1.prowlarr_configs.metadata.0.name}
    accessMode: ReadWriteOnce
    size: 2Gi
EOF
  ]
}
