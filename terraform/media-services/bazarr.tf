#########################################################################
################################ BAZARR #################################
#########################################################################

resource "kubernetes_persistent_volume_v1" "bazarr_configs" {
  metadata {
    name = "bazarr-configs"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "5Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.bazarr.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim_v1" "bazarr_configs" {
  metadata {
    name = "bazarr-config"
  }
  spec {
    volume_name        = kubernetes_persistent_volume_v1.bazarr_configs.metadata.0.name
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "helm_release" "bazarr" {
  name       = "bazarr"
  chart      = "bazarr"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "400"
  version    = var.versions.bazarr.chart

  values = [<<EOF
initContainers:
  update-volume-permission:
    image: busybox
    command: ["sh", "-c", "chmod -R 777 /config"]
    volumeMounts:
    - name: config
      mountPath: /config
    securityContext:
      runAsUser: 0
image:
  repository: linuxserver/bazarr
  tag: ${var.versions.bazarr.image}
resources:
  limits:
    cpu: 350m
    memory: 550Mi
  requests:
    cpu: 300m
    memory: 450Mi
ingress:
  main:
    enabled: true
    annotations: 
      kubernetes.io/ingress.class: nginx
    hosts:
      - host: bazarr.${data.terraform_remote_state.infra.outputs.local_domain}
        paths:
          - path: /
persistence:
  config:
    enabled: true
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${kubernetes_persistent_volume_claim_v1.bazarr_configs.metadata.0.name}
    accessMode: ReadWriteOnce
    size: 5Gi
  media:
    enabled: true
    emptyDir: false
    mountPath: /downloads
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
    accessMode: ReadWriteMany
probes:
  startup:
    enabled: true
    spec:
      periodSeconds: 15
      failureThreshold: 40
EOF
  ]
}
