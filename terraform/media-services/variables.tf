variable "versions" {
  type = object({
    radarr = object({
      chart = string
      image = string
    })
    readarr = object({
      chart = string
      image = string
    })
    sonarr = object({
      chart = string
      image = string
    })
    jellyfin = object({
      chart = string
      image = string
    })
    overseerr = object({
      chart = string
      image = string
    })
    prowlarr = object({
      image = string
      chart = string
    })
    bazarr = object({
      image = string
      chart = string
    })
    transmission = object({
      image = string
    })
    mealie = object({
      image = string
    })
    tdarr = object({
      chart = string
      image = string
    })
  })
}
