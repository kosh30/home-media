versions = {
  radarr = {
    chart = "16.3.2"
    image = "5.2.6" ### https://fleet.linuxserver.io/image?name=linuxserver/radarr  ### https://hub.docker.com/r/linuxserver/radarr/tags
  }
  readarr = {
    chart = "6.4.2"
    image = "0.3.4-nightly" ### https://fleet.linuxserver.io/image?name=linuxserver/readarr  ### https://hub.docker.com/r/linuxserver/readarr/tags
  }
  sonarr = {
    chart = "16.3.2"
    image = "4.0.1" ### https://fleet.linuxserver.io/image?name=linuxserver/sonarr ### https://hub.docker.com/r/linuxserver/sonarr/tags
  }
  jellyfin = {
    chart = "9.5.3"
    image = "10.8.13" ### https://fleet.linuxserver.io/image?name=linuxserver/jellyfin ### https://hub.docker.com/r/jellyfin/plex/tags
  }
  overseerr = {
    chart = "5.4.2"
    image = "1.33.2" ### https://github.com/sct/overseerr/releases
  }
  prowlarr = {
    chart = "4.5.2"
    image = "1.12.2" ### https://fleet.linuxserver.io/image?name=linuxserver/prowlarr ### https://hub.docker.com/r/linuxserver/prowlarr/tags
  }
  bazarr = {
    chart = "10.6.2"
    image = "1.4.0" ### https://fleet.linuxserver.io/image?name=linuxserver/bazarr
  }
  transmission = {
    image = "4.0.5" ### https://hub.docker.com/r/linuxserver/transmission/tags
  }
  mealie = {
    #image = "v1.0.0beta-5" ### https://github.com/mealie-recipes/mealie/tags ### https://hub.docker.com/r/hkotel/mealie/tags
    image = "v1.0.0" ### https://github.com/mealie-recipes/mealie/tags ### https://hub.docker.com/r/hkotel/mealie/tags
  }
  tdarr = {
    chart = "4.6.2"
    image = "2.11.01"
  }
}
