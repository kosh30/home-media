#########################################################################
################################ MEALIE #################################
#########################################################################

resource "kubernetes_persistent_volume_v1" "mealie" {
  metadata {
    name = "mealie"
  }
  spec {
    capacity = {
      storage = "5Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.mealie.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim_v1" "mealie" {
  metadata {
    name = "mealie"
  }
  spec {
    volume_name        = kubernetes_persistent_volume_v1.mealie.metadata.0.name
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_deployment_v1" "mealie" {
  metadata {
    name = "mealie"
    labels = {
      app = "mealie"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "mealie"
      }
    }
    template {
      metadata {
        labels = {
          app = "mealie"
        }
      }
      spec {
        container {
          image = "hkotel/mealie:${var.versions.mealie.image}"
          name  = "mealie"

          resources {
            limits = {
              cpu    = "500m"
              memory = "400Mi"
            }
            requests = {
              cpu    = "400m"
              memory = "300Mi"
            }
          }
          liveness_probe {
            http_get {
              port = 9000
              path = "/"
            }
            period_seconds    = 5
            failure_threshold = 40
          }
          dynamic "env" {
            for_each = {
              API_PORT                    = "9000"
              BASE_URL                    = "https://mealie.vegabookholt.uk"
              DEFAULT_EMAIL               = "sergiojvg92@gmail.com"
              SECURITY_MAX_LOGIN_ATTEMPTS = "100"
              SECURITY_USER_LOCKOUT_TIME  = "1"
              SMTP_AUTH_STRATEGY          = "TLS"
              SMTP_FROM_EMAIL             = "bioelectronicnavigator2@gmail.com"
              SMTP_FROM_NAME              = "bioelectronicnavigator2@gmail.com"
              SMTP_HOST                   = "smtp.gmail.com"
              SMTP_PASSWORD               = data.sops_file.secrets.data["ben_email_password"]
              SMTP_PORT                   = "587"
              SMTP_USER                   = "bioelectronicnavigator2@gmail.com"
              TZ                          = "CET"
            }
            content {
              name  = env.key
              value = env.value
            }
          }
          volume_mount {
            mount_path = "/app/data"
            name       = "data"
          }
        }
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.mealie.metadata.0.name
          }
        }
      }
    }
  }
  timeouts {
    create = "1m"
    delete = "1m"
    update = "1m"
  }
}

resource "kubernetes_service" "mealie" {
  metadata {
    name = "mealie"
  }
  spec {
    selector = {
      app = "mealie"
    }
    port {
      port        = 9000
      target_port = 9000
    }
  }
  timeouts {
    create = "1m"
  }
}
