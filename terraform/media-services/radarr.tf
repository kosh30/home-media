#########################################################################
################################ RADARR #################################
#########################################################################
#resource "kubernetes_config_map" "radarr-config" {
#  metadata {
#    name = "radarr-config"
#  }
#
#  data = {
#    "config.xml" = "${local.radarr_config}"
#  }
#}


resource "kubernetes_persistent_volume" "radarr_configs" {
  metadata {
    name = "radarr-configs"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "5Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.radarr.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim" "radarr_configs" {
  metadata {
    name = "radarr-config"
  }
  spec {
    volume_name        = kubernetes_persistent_volume.radarr_configs.metadata.0.name
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "helm_release" "radarr" {
  name       = "radarr"
  chart      = "radarr"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "300"
  version    = var.versions.radarr.chart

  values = [<<EOF
image:
  repository: linuxserver/radarr
  tag: ${var.versions.radarr.image}
resources:
  limits:
    cpu: 600m
    memory: 450Mi
  requests:
    cpu: 500m
    memory: 400Mi
priorityClassName: ${data.terraform_remote_state.infra.outputs.important_priority_class}
ingress:
  main:
    enabled: true
    annotations: 
      kubernetes.io/ingress.class: nginx
    hosts:
      - host: radarr.${data.terraform_remote_state.infra.outputs.local_domain}
        paths:
          - path: /
persistence:
  config:
    enabled: true
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${kubernetes_persistent_volume_claim.radarr_configs.metadata.0.name}
    accessMode: ReadWriteOnce
    size: 5Gi
  media:
    enabled: true
    emptyDir: false
    mountPath: /downloads
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
    accessMode: ReadWriteMany
EOF
  ]
}
