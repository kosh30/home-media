resource "kubernetes_persistent_volume" "jellyfin_configs" {
  metadata {
    name = "jellyfin-configs"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "20Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.jellyfin.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim" "jellyfin_configs" {
  metadata {
    name = "jellyfin"
  }
  spec {
    volume_name        = kubernetes_persistent_volume.jellyfin_configs.metadata.0.name
    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "20Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "helm_release" "jellyfin" {
  name       = "jellyfin"
  chart      = "jellyfin"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "200"
  version    = var.versions.jellyfin.chart

  values = [<<EOF
image:
  repository: linuxserver/jellyfin
  tag: ${var.versions.jellyfin.image}
env:
  TZ: ${data.terraform_remote_state.infra.outputs.timezone}
priorityClassName: ${data.terraform_remote_state.infra.outputs.important_priority_class}
ingress:
  main:
    enabled: true
    annotations: 
      kubernetes.io/ingress.class: nginx
    hosts:
      - host: jellyfin.${data.terraform_remote_state.infra.outputs.local_domain}
        paths:
          - path: /
resources:
  limits:
    cpu: 2500m
    memory: 2000Mi
  requests:
    cpu: 1500m
    memory: 1200Mi
probes:
  liveness:
    enabled: true
    # -- Set this to `true` if you wish to specify your own livenessProbe
    custom: false
    spec:
      initialDelaySeconds: 0
      periodSeconds: 10
      timeoutSeconds: 1
      failureThreshold: 8
  readiness:
    enabled: true
    # -- Set this to `true` if you wish to specify your own readinessProbe
    custom: false
    spec:
      initialDelaySeconds: 0
      periodSeconds: 10
      timeoutSeconds: 1
      failureThreshold: 5
persistence:
  media:
    enabled: true
    emptyDir: false
    mountPath: /downloads
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
    accessMode: ReadWriteMany
  config:
    enabled: true
    mountPath: /config
    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
    existingClaim: ${kubernetes_persistent_volume_claim.jellyfin_configs.metadata.0.name}
    accessMode: ReadWriteOnce
    size: 100Gi
EOF
  ]
}

resource "kubernetes_ingress_v1" "jellyfin_external" {
  metadata {
    name = "jellyfin-external"
    annotations = {
      "kubernetes.io/ingress.class"    = "nginx"
      "cert-manager.io/cluster-issuer" = data.terraform_remote_state.infra.outputs.certmanager_clusterissuer_name
      "nginx.org/redirect-to-https"    = true
    }
  }
  spec {
    tls {
      secret_name = data.terraform_remote_state.infra.outputs.wildcard_ssl_certificate_secret_name
      hosts       = ["jellyfin.${data.terraform_remote_state.infra.outputs.public_domain}"]
    }
    rule {
      host = "jellyfin.${data.terraform_remote_state.infra.outputs.public_domain}"
      http {
        path {
          backend {
            service {
              name = "jellyfin"
              port {
                number = 8096
              }
            }
          }
          path = "/"
        }
      }
    }
  }
  depends_on = [helm_release.jellyfin]
}

resource "kubernetes_ingress_v1" "jellyfin_internal" {
  metadata {
    name = "jellyfin-internal"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      #"cert-manager.io/cluster-issuer" = data.terraform_remote_state.infra.outputs.certmanager_clusterissuer_name
      #"nginx.org/redirect-to-https"    = false
    }
  }
  spec {
    rule {
      host = "jellyfin.${data.terraform_remote_state.infra.outputs.local_domain}"
      http {
        path {
          backend {
            service {
              name = "jellyfin"
              port {
                number = 8096
              }
            }
          }
          path = "/"
        }
      }
    }
  }
  depends_on = [helm_release.jellyfin]
}
