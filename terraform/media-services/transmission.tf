#########################################################################
################################# TRANSMISSION ##########################
#########################################################################

#  tag: ${var.versions.adguard.image}

resource "kubernetes_config_map_v1" "transmission_config" {
  metadata {
    name = "transmission-config"
  }

  data = {
    "settings.json" = <<EOF
{
    "alt-speed-down": 50,
    "alt-speed-enabled": true,
    "alt-speed-time-begin": 540,
    "alt-speed-time-day": 127,
    "alt-speed-time-enabled": false,
    "alt-speed-time-end": 1020,
    "alt-speed-up": 5000,
    "announce-ip": "",
    "announce-ip-enabled": false,
    "anti-brute-force-enabled": false,
    "anti-brute-force-threshold": 100,
    "bind-address-ipv4": "0.0.0.0",
    "bind-address-ipv6": "::",
    "blocklist-enabled": false,
    "blocklist-url": "http://www.example.com/blocklist",
    "cache-size-mb": 64,
    "default-trackers": "",
    "dht-enabled": true,
    "download-dir": "/downloads/complete",
    "download-queue-enabled": true,
    "download-queue-size": 5,
    "encryption": 1,
    "idle-seeding-limit": 30,
    "idle-seeding-limit-enabled": false,
    "incomplete-dir": "/downloads/incomplete",
    "incomplete-dir-enabled": true,
    "lpd-enabled": false,
    "message-level": 2,
    "peer-congestion-algorithm": "",
    "peer-id-ttl-hours": 6,
    "peer-limit-global": 200,
    "peer-limit-per-torrent": 50,
    "peer-port": 51413,
    "peer-port-random-high": 65535,
    "peer-port-random-low": 49152,
    "peer-port-random-on-start": false,
    "peer-socket-tos": "le",
    "pex-enabled": true,
    "port-forwarding-enabled": true,
    "preallocation": 1,
    "prefetch-enabled": true,
    "queue-stalled-enabled": true,
    "queue-stalled-minutes": 30,
    "ratio-limit": 2,
    "ratio-limit-enabled": false,
    "rename-partial-files": true,
    "rpc-authentication-required": false,
    "rpc-bind-address": "0.0.0.0",
    "rpc-enabled": true,
    "rpc-host-whitelist": "",
    "rpc-host-whitelist-enabled": false,
    "rpc-password": "{08c686ccb3669e811593657d84f825f2deee25b6xS83yWt/",
    "rpc-port": 9091,
    "rpc-socket-mode": "0750",
    "rpc-url": "/transmission/",
    "rpc-username": "",
    "rpc-whitelist": "",
    "rpc-whitelist-enabled": false,
    "scrape-paused-torrents-enabled": true,
    "script-torrent-added-enabled": false,
    "script-torrent-added-filename": "",
    "script-torrent-done-enabled": false,
    "script-torrent-done-filename": "",
    "script-torrent-done-seeding-enabled": false,
    "script-torrent-done-seeding-filename": "",
    "seed-queue-enabled": false,
    "seed-queue-size": 10,
    "speed-limit-down": 5000,
    "speed-limit-down-enabled": true,
    "speed-limit-up": 10,
    "speed-limit-up-enabled": true,
    "start-added-torrents": true,
    "tcp-enabled": true,
    "torrent-added-verify-mode": "fast",
    "trash-original-torrent-files": false,
    "umask": "002",
    "upload-slots-per-torrent": 14,
    "utp-enabled": false,
    "watch-dir": "/watch",
    "watch-dir-enabled": false
}
EOF
  }
}

#resource "kubernetes_persistent_volume_claim" "rtorrent" {
#  metadata {
#    name = "exampleclaimname"
#  }
#  spec {
#    access_modes = ["ReadWriteMany"]
#    resources {
#      requests = {
#        storage = "5Gi"
#      }
#    }
#    volume_name = "${kubernetes_persistent_volume.example.metadata.0.name}"
#  }
#}

resource "kubernetes_deployment_v1" "transmission" {
  metadata {
    name = "transmission"
    labels = {
      app = "transmission"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "transmission"
      }
    }
    template {
      metadata {
        labels = {
          app = "transmission"
        }
      }
      spec {
        container {
          image   = "linuxserver/transmission:${var.versions.transmission.image}"
          name    = "transmission"
          command = ["/bin/bash", "-c"]
          args    = ["cp /temp-config/settings.json /config/settings.json && /init"]

          resources {
            limits = {
              cpu    = "800m"
              memory = "900Mi"
            }
            requests = {
              cpu    = "500m"
              memory = "756Mi"
            }
          }
          liveness_probe {
            tcp_socket {
              port = 9091
            }
          }
          readiness_probe {
            tcp_socket {
              port = 9091
            }
          }
          volume_mount {
            mount_path = "/temp-config"
            name       = "transmission-config"
          }
          volume_mount {
            mount_path = "/downloads"
            name       = "downloads"
          }
        }
        volume {
          name = "transmission-config"
          config_map {
            name         = kubernetes_config_map_v1.transmission_config.metadata.0.name
            default_mode = "0777"
          }
        }
        volume {
          name = "downloads"
          persistent_volume_claim {
            claim_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc
          }
        }
      }
    }
  }
  timeouts {
    create = "2m"
  }
}

resource "kubernetes_service" "transmission" {
  metadata {
    name = "transmission"
  }
  spec {
    selector = {
      app = "transmission"
    }
    port {
      port        = 9091
      target_port = 9091
    }
  }
  timeouts {
    create = "1m"
  }
}
