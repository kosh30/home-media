#########################################################################
########################### HOME ASSISTANT ##############################
#########################################################################

resource "kubernetes_persistent_volume_v1" "homeassistant_configs" {
  metadata {
    name = "homeassistant-configs"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "20Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata.0.name
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.homeassistant.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim_v1" "homeassistant_configs" {
  metadata {
    name = "homeassistant-config"
  }
  spec {
    volume_name        = kubernetes_persistent_volume_v1.homeassistant_configs.metadata.0.name
    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata.0.name
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "20Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}


resource "helm_release" "homeassistant" {
  name       = "homeassistant"
  chart      = "home-assistant"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "1500"
  version    = var.versions.homeassistant.chart

  values = [<<EOF
image:
  #repository: homeassistant/home-assistant
  tag: ${var.versions.homeassistant.image}
hostNetwork: true
dnsPolicy: ClusterFirstWithHostNet
securityContext:
  privileged:  true
priorityClassName: ${kubernetes_priority_class_v1.important.metadata.0.name}
persistence:
  config:
    enabled: true
    storageClass: ${kubernetes_storage_class.nfs_dummy.metadata.0.name}
    existingClaim: ${kubernetes_persistent_volume_claim_v1.homeassistant_configs.metadata.0.name}
    accessMode: ReadWriteOnce
    size: 20Gi
  usb:
    enabled: true
    type: hostPath
    hostPath: /dev/serial/by-id/usb-Silicon_Labs_Sonoff_Zigbee_3.0_USB_Dongle_Plus_0001-if00-port0
    mountPath: /dev/ttyUSB0
nodeSelector:
  kubernetes.io/hostname: slave2
resources:
  limits:
    cpu: 1000m
    memory: 1200Mi
  requests:
    cpu: 901m
    memory: 1200Mi
addons:
  codeserver:
    enabled: true
    args:
    - --auth
    - none
    - --user-data-dir
    - "/config/.vscode"

    workingDir: "/config"

    volumeMounts:
    - name: config
      mountPath: /config
EOF
  ]
}

resource "kubernetes_ingress_v1" "homeassistant_home" {
  metadata {
    name = "homeassistant-home"
    annotations = {
      "kubernetes.io/ingress.class"  = "nginx"
      "nginx.org/websocket-services" = "homeassistant-home-assistant"
    }
  }
  spec {
    rule {
      host = "ha.${var.local_domain}"
      http {
        path {
          backend {
            service {
              name = "homeassistant-home-assistant"
              port {
                number = 8123
              }
            }
          }
          path = "/"
        }
      }
    }
  }
  depends_on = [helm_release.homeassistant]
}

resource "kubernetes_ingress_v1" "homeassistant_external" {
  metadata {
    name = "homeassistant-external"
    annotations = {
      "kubernetes.io/ingress.class"    = "nginx"
      "cert-manager.io/cluster-issuer" = kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
      "nginx.org/redirect-to-https"    = true
      "nginx.org/websocket-services"   = "homeassistant-home-assistant"
    }
  }
  spec {
    tls {
      secret_name = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
      hosts       = ["homeassistant.${var.public_domain}"]
    }
    rule {
      host = "homeassistant.${var.public_domain}"
      http {
        path {
          backend {
            service {
              name = "homeassistant-home-assistant"
              port {
                number = 8123
              }
            }
          }
          path = "/"
        }
      }
    }
  }
  depends_on = [helm_release.homeassistant]
}

resource "kubernetes_ingress_v1" "homeassistant-vscode" {
  metadata {
    name = "homeassistant-vscode"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      host = "code.ha.${var.local_domain}"
      http {
        path {
          backend {
            service {
              name = "homeassistant-home-assistant-codeserver"
              port {
                number = 12321
              }
            }
          }
          path = "/"
        }
      }
    }
  }
  depends_on = [helm_release.homeassistant]
}

##########################################################################
################################# ESPHOME ################################
##########################################################################
#
#resource "helm_release" "esphome" {
#  name       = "esphome"
#  chart      = "esphome"
#  repository = "https://k8s-at-home.com/charts/"
#  timeout    = "400"
#  namespace  = "default"
#  version    = "8.2.0"
#
#  values = [<<EOF
#image:
#  #repository: esphome/esphome
#  tag: 2022.2.6
#hostNetwork: true
#resources:
#  limits:
#    cpu: 1500m
#    memory: 1024Mi
#  requests:
#    cpu: 700m
#    memory: 300Mi
#ingress:
#  main:
#    enabled: true
#    hosts:
#      - host: esp.${var.domain}
#        paths:
#          - path: /
#persistence:
#  config:
#    enabled: true
#    storageClass: ${local.sc_name}
#    accessMode: ReadWriteMany
#    skipuninstall: true
#    size: 2Gi
#nodeSelector:
#  kubernetes.io/hostname: master
#EOF
#  ]
#  depends_on = [
#    helm_release.qbittorrent
#  ]
#}
