output "wildcard_ssl_certificate_secret_name" {
  value       = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
  description = "Name of the secret hosting the wildcard SSL certificate"
}

output "certmanager_clusterissuer_name" {
  value       = kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
  description = "Name of the cluster issuer resource used to generate SSL certificates"
}

output "nfs_multimedia_storage_class" {
  value       = kubernetes_storage_class.nfs_dummy.metadata.0.name
  description = "Name of the storage class used by the NFS (multimedia) PV and PVC"
}

output "nfs_multimedia_pvc" {
  value       = kubernetes_persistent_volume_claim.multimedia.metadata.0.name
  description = "Name of the PVC pointing to the NFS drive"
}

output "public_domain" {
  value       = var.public_domain
  description = "Public domain used for services in this cluster"
}

output "public_ip" {
  value       = var.public_ip
  description = "Public IP used for this cluster"
}

output "local_domain" {
  value       = var.local_domain
  description = "Local domain used for the services in this cluster (only accessible from the LAN (because private IPs are used) and it is only resolved by Adguard)"
}

output "timezone" {
  value       = var.timezone
  description = "Timezone in this format: https://www.php.net/manual/en/timezones.php"
}

output "important_priority_class" {
  value       = kubernetes_priority_class_v1.important.metadata.0.name
  description = "Name of the priority class to use for important services. Recommended for services with a high resources request so it can evict other pods"
}
