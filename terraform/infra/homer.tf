##########################################################################
################################## HOMER #################################
##########################################################################
resource "helm_release" "homer" {
  name       = "homer"
  chart      = "homer"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "450"
  version    = var.versions.homer.chart

  values = [<<EOF
image:
  repository: b4bz/homer
  tag: ${var.versions.homer.image}
resources:
  limits:
    cpu: 80m
    memory: 50Mi
  requests:
    cpu: 80m
    memory: 50Mi
ingress:
  main:
    enabled: false
    annotations: 
      kubernetes.io/ingress.class: nginx
    hosts:
      - host: homer.${var.public_domain}
        paths:
          - path: /
configmap:
  config:
    # -- Store homer configuration as a ConfigMap
    enabled: true
    # -- Homer configuration. See [image documentation](https://github.com/bastienwirtz/homer/blob/main/docs/configuration.md) for more information.
    # @default -- See values.yaml
    data:
      config.yml: |
        title: "Home"
        # subtitle: "Home"
        # documentTitle: "Welcome" # Customize the browser tab text
        logo: "assets/logo.png"
        # Alternatively a fa icon can be provided:
        # icon: "fas fa-skull-crossbones"

        header: false # Set to false to hide the header
        footer: 'All services related to the house'

        columns: "3" # "auto" or number (must be a factor of 12: 1, 2, 3, 4, 6, 12)
        connectivityCheck: true # whether you want to display a message when the apps are not accessible anymore (VPN disconnected for example)

        # Optional: Proxy / hosting option
        proxy:
          useCredentials: true # send cookies & authorization headers when fetching service specific data. Set to `true` if you use an authentication proxy. Can be overrided on service level.

        # Set the default layout and color scheme
        defaults:
          layout: columns # Either 'columns', or 'list'
          colorTheme: auto # One of 'auto', 'light', or 'dark'

        # Optional theming
        theme: default # 'default' or one of the themes available in 'src/assets/themes'.

        # Here is the exhaustive list of customization parameters
        # However all value are optional and will fallback to default if not set.
        # if you want to change only some of the colors, feel free to remove all unused key.
        colors:
          light:
            highlight-primary: "#3367d6"
            highlight-secondary: "#4285f4"
            highlight-hover: "#5a95f5"
            background: "#f5f5f5"
            card-background: "#ffffff"
            text: "#363636"
            text-header: "#424242"
            text-title: "#303030"
            text-subtitle: "#424242"
            card-shadow: rgba(0, 0, 0, 0.1)
            link: "#3273dc"
            link-hover: "#363636"
            background-image: "assets/your/light/bg.png"
          dark:
            highlight-primary: "#3367d6"
            highlight-secondary: "#4285f4"
            highlight-hover: "#5a95f5"
            background: "#131313"
            card-background: "#2b2b2b"
            text: "#eaeaea"
            text-header: "#ffffff"
            text-title: "#fafafa"
            text-subtitle: "#f5f5f5"
            card-shadow: rgba(0, 0, 0, 0.4)
            link: "#3273dc"
            link-hover: "#ffdd57"
            background-image: "assets/your/dark/bg.png"

        services:
          - name: "Media"
            icon: "fas fa-eye"
            items:
              - name: "Jellyfin"
                icon: "fas fa-eye"
                subtitle: "Media Server"
                tag: "media"
                url: "https://jellyfin.${var.public_domain}"
                target: "_blank" # optional html tag target attribute
              - name: "Overseer"
                icon: "fas fa-photo-video"
                subtitle: "Find Movies and Series"
                tag: "media"
                url: "https://overseerr.${var.public_domain}"
                target: "_blank" # optional html tag target attribute
              - name: "Radarr"
                icon: "fas fa-file-import"
                subtitle: "Search for movies"
                tag: "media"
                url: "https://radarr.${var.public_domain}"
                #endpoint: "https://radarr.my.house"
                #type: "Radarr"
                #apikey: "${data.sops_file.secrets.data["radarr_api_key"]}"
              - name: "Sonarr"
                icon: "fas fa-file-import"
                subtitle: "Search for series"
                tag: "media"
                url: "https://sonarr.${var.public_domain}"
                #endpoint: "http://sonarr:8989"
                #type: "Sonarr"
                #apikey: "${data.sops_file.secrets.data["sonarr_api_key"]}"
              - name: "Prowlarr"
                icon: "fas fa-file-medical-alt"
                subtitle: "Index torrents"
                tag: "media"
                url: "https://prowlarr.${var.public_domain}"
                endpoint: "http://prowlarr:9696/"
                #type: "Prowlarr"
                #apikey: "${data.sops_file.secrets.data["prowlarr_api_key"]}"
              - name: "Bazarr"
                icon: "fas fa-text-height"
                subtitle: "Download Subtitles"
                tag: "media"
                url: "https://bazarr.${var.public_domain}"
                #type: "Prowlarr"
                #apikey: "${data.sops_file.secrets.data["prowlarr_api_key"]}"
              - name: "Transmission"
                icon: "fas fa-download"
                subtitle: "Get torrents"
                tag: "media"
                url: "https://transmission.${var.public_domain}"
          - name: "Services"
            icon: "fas fa-random"
            items:
              - name: "Weather"
                location: "Utrecht" # your location.
                locationId: "2745912" 
                apikey: "" 
                units: "metric" # units to display temperature. Can be one of: metric, imperial, kelvin. Defaults to kelvin.
                background: "square" # choose which type of background you want behind the image. Can be one of: square, circle, none. Defaults to none.
                type: "OpenWeather"
              - name: "Home Assistant"
                icon: "fas fa-home"
                subtitle: "Smart home hub"
                tag: "smart"
                url: "https://homeassistant.${var.public_domain}"
              - name: "Home Assistant Configs"
                icon: "fas fa-file-code"
                subtitle: "Home Assistant Configs"
                tag: "smart"
                url: "https://code.${var.public_domain}"
              - name: "Mealie"
                icon: "fas fa-hamburger"
                subtitle: "Recipees"
                tag: "food"
                url: "https://mealie.${var.public_domain}"
              - name: "File Browser"
                icon: "fas fa-file"
                subtitle: "File Manager"
                tag: "files"
                url: "https://files.${var.public_domain}"
          - name: "Infrastructure"
            icon: "fas fa-server"
            items:
              - name: "External DNS"
                icon: "fas fa-cloud"
                subtitle: "Cloudflare External DNS"
                tag: "dns"
                url: "https://dash.cloudflare.com/1e527ff37505b7cdca923c080e9b8042/vegabookholt.uk/dns/records"
              - name: "Cloudflare Applications DNS"
                icon: "fas fa-cloud"
                subtitle: "Cloudflare ZeroTrust Applications"
                tag: "security"
                url: "https://one.dash.cloudflare.com/1e527ff37505b7cdca923c080e9b8042/access/apps"
              - name: "Qnap"
                icon: "fas fa-coins"
                subtitle: "QNAP"
                tag: "storage"
                url: "https://longhorn.${var.public_domain}"
              - name: "AdGuard"
                icon: "fas fa-globe-americas"
                subtitle: "Monitoring system"
                tag: "dns"
                url: "https://adguard.${var.public_domain}"
                endpoint: 
                type: "AdGuardHome"
              - name: "Prometheus"
                icon: "far fa-chart-bar"
                subtitle: "Monitoring system"
                tag: "monitoring"
                url: "https://prometheus.${var.public_domain}"
                type: "Prometheus"
              - name: "Alert Manager"
                icon: "fas fa-exclamation"
                subtitle: "Monitoring system"
                tag: "monitoring"
                url: "https://alertmanager.${var.public_domain}"
              - name: "Blackbox Exporter"
                icon: "fas fa-exclamation"
                subtitle: "Prometheus Blackbox Exporter"
                tag: "monitoring"
                url: "https://blackbox-exporter.${var.public_domain}"
              - name: "Local Grafana"
                icon: "fas fa-chart-area"
                subtitle: "Monitoring system"
                tag: "monitoring"
                url: "https://grafana.${var.public_domain}"
              - name: "Grafana Cloud"
                icon: "fas fa-chart-area"
                subtitle: "Monitoring system"
                tag: "monitoring"
                url: "https://sergiojvg92.grafana.net/"
              #- name: "Uptimerobot"
              #  icon: "far fa-chart-bar"
              #  subtitle: "Monitoring system"
              #  tag: "monitoring"
              #  url: "https://uptimerobot.com/dashboard#mainDashboard"
              - name: "CI/CD Pipeline"
                icon: "fab fa-gitlab"
                subtitle: "Deployment pipelines"
                tag: "cicd"
                url: "https://gitlab.com/sergiojvg/home-media/-/pipelines"
EOF
  ]
}

resource "kubernetes_ingress_v1" "homer_external" {
  metadata {
    name = "homer-external"
    annotations = {
      "kubernetes.io/ingress.class"    = "nginx"
      "cert-manager.io/cluster-issuer" = kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
      "nginx.org/redirect-to-https"    = true
      #"nginx.org/websocket-services"   = "homeassistant-home-assistant"
    }
  }
  spec {
    tls {
      secret_name = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
      hosts       = ["homer2.${var.public_domain}"]
    }
    rule {
      host = "homer2.${var.public_domain}"
      http {
        path {
          backend {
            service {
              name = "homer"
              port {
                number = 8080
              }
            }
          }
          path = "/"
        }
      }
    }
  }
  depends_on = [helm_release.homer]
}
