resource "helm_release" "nginx" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_agent_token"] == "" ? 0 : 1
  name       = "nginx-ingress"
  chart      = "nginx-ingress"
  repository = "https://helm.nginx.com/stable"
  namespace  = "default"
  version    = var.versions.nginx.chart
  timeout    = "100"

  values = [<<EOF
controller:
  healthStatus: true
  healthStatusURI: "/ping"
  enableLatencyMetrics: true
  stats:
    enabled: true
  metrics:
    enabled: true
    service:
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/port: "10254"
  resources:
    requests:
      cpu: 150m
      memory: 128Mi
    limits:
      cpu: 400m
      memory: 600Mi
  serviceMonitor:
    create: true
  service:
    #loadBalancerIP: 192.168.1.221
    type: NodePort
    externalTrafficPolicy: Cluster ### Local preserves the IP of the client, but nginx would have to be ran as a daemonset 
    httpPort:
      nodePort: 30080
    httpsPort:
      nodePort: 30443
    #annotations: 
    #  metallb.universe.tf/loadBalancerIPs: ${var.ingress_ip}
prometheus:
  create: true
  port: 9113
  scheme: http
EOF
  ]
  depends_on = [
    kubernetes_manifest.reserved_address_pool
  ]
}
