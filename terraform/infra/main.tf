/**
 * This terraform module will set all the dependencies for the entire project and configure some infra related services.
 * The services installed and configured by this module are:
 * * [Longhorn](https://longhorn.io/)
 * * [HomeAssistant](https://www.home-assistant.io/)
 * * [AdGuard](https://adguard.com/en/welcome.html)
 * * [Cert-Manager](https://cert-manager.io/docs/)
 * * [File Browser](https://filebrowser.org/)
 * * [Homer](https://github.com/bastienwirtz/homer)
 * * [Gitlab Agent](https://docs.gitlab.com/ee/user/clusters/agent/) -- useful for setting up Gitlab CICD pipelines
 * * Traefik basic auth middleware
 *
 * Besides installing and configuring these services, these other services will also be configured:
 * * Optionally: Set up your external DNS configs in Cloudflare (there are some requirements for this)
 * * Optionally: Set up [Cloudflare's Zero Trust](https://developers.cloudflare.com/cloudflare-one/) for the services you publish behind a [Cloudflare's Tunnel](https://www.cloudflare.com/products/tunnel/)
 * * Optionally: Set up ingress resources for your external NFS appliance like a qnap (in case you have one, hence the "optional")
 *
 * This terraform root module should be deployed first, since this module sets up services that are required by the other modules. Also,
 * other modules will read the outputs from this module using [terraform_remote_state Data Sources](https://developer.hashicorp.com/terraform/language/state/remote-state-data).
 *
 * This module might fail due to timeouts depending on your infrastructure. In that case, you should be fine by running it again.
 *
 * ## Requirements:
 *
 * ### External DNS and cloudflare
 *
 * If setting up your external DNS (only cloudflare is supported in this repo), you need a Cloudflare account with your domain already added (and Cloudflare also needs to be the authoritative DNS for your domain).
 * Also, a cloudflare API key is required to configure the cloudflare provider.
 *
 * *NOTE:* This is only optional, you can also configure your external DNS records (using [DuckDNS](https://www.duckdns.org/) for free for example. In that case, set your domain in *public_domain* (duckdns.org for example) and set
 * *configure_external_dns* to false)
 *
 * ### Secrets
 * Sensitive information is encrypted. You need to fill out a few settings this way. To do so, configure secrets-decrypted-sample:
 *
 * ```
 * {
 *   "cloudflare_api_key": "",
 *   "google_cloudflare_oauth_client_id": "",
 *   "google_cloudflare_oauth_client_secret": "",
 *   "gitlab_kubernetes_agent_token": "",
 *   "prowlarr_api_key": "",
 *   "radarr_api_key": "",
 *   "sonarr_api_key": "",
 *   "traefik_basic_auth_user": "",
 *   "traefik_basic_auth_password": ""
 * }
 * ```
 *
 * Radarr, Sonarr and Prowlarr API keys can be left empty for the moment. You can fill those out after those services are installed and configured (they are installed by a different terraform root module). <br/>
 * Those API keys are only used to set up the integration with Homer, so they are not even important. <br/>
 * **gitlab_kubernetes_agent_token** contains a token to register your cluster in your gitlab project. If this token is left empty ("") then the agent will not be installed. <br/>
 * To generate this token follow [these instructions](https://docs.gitlab.com/ee/user/clusters/agent/install/#register-the-agent-with-gitlab) <br/>
 * **cludflare_api_key** is used to set up your external DNS records and argo tunnels. It can be left empty if configure_external_dns is set to false <br/>
 * **google_cloudflare_oauth_client_id** and **google_cloudflare_oauth_client_secret** Besides using an ingress and using an A record to point to your cluster's public IP, [Cloudflare tunnels](https://www.cloudflare.com/products/tunnel/) can be used for free. On top of that, Cloudflare Zero trust is used to secure your apps using [Cloudflare Applications](https://developers.cloudflare.com/cloudflare-one/applications/) and a [Google identity provider](https://developers.cloudflare.com/cloudflare-one/identity/idp-integration/google/) is used to grant access to your apps. These vars define the ID and secret from your Google OAuth configuration for this client (more info in Cloudflare's docs) <br/>
 * **traefik_basic_auth_user** and **traefik_basic_auth_password** Some endpoints will require basic auth (configured through traefik). These are the credentials
 *
 *
 *
 * To encrypt this file (assuming you installed sops and generated a key file as explained [here](https://gitlab.com/sergiojvg/home-media/-/tree/main/terraform/infra#secrets)), run:
 *
 * ```
 * sops --encrypt --age $YOUR_SOPS_PUBLIC_KEY secrets-decrypted-sample > secrets.json
 * ```
 * If secrets.json is missing, this module will fail
 *
 * ### Terraform variables
 *
 * Configure the terraform variables described in this module. *terraform.tfvars* is a good place to do so
 *
 * ## Debugging
 * Longhorn can take a lot of time to be fully installed. The helm chart will report it as ready before all the pods are running. To check the status, run:
 *
 * ```
 * kubectl get pod -n longhorn-system
 * ```
 *
 */

data "sops_file" "secrets" {
  source_file = "secrets.json"
}

resource "kubernetes_storage_class" "nfs_dummy" {
  metadata {
    name = "nfs-dummy"
  }
  reclaim_policy      = "Retain"
  storage_provisioner = "nfs"
}

resource "kubernetes_persistent_volume" "multimedia" {
  metadata {
    name = "multimedia"
  }
  spec {
    capacity = {
      storage = var.media_size
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata[0].name
    mount_options      = ["nfsvers=4.0"]
    persistent_volume_source {
      nfs {
        server = var.nfs_server
        path   = "/Shared"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim" "multimedia" {
  metadata {
    name = "multimedia"
  }
  spec {
    volume_name        = kubernetes_persistent_volume.multimedia.metadata.0.name
    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata[0].name
    access_modes       = ["ReadWriteMany"]
    resources {
      requests = {
        storage = var.media_size
      }
    }
  }
  depends_on = [kubernetes_persistent_volume.multimedia]
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_priority_class_v1" "default" {
  metadata {
    name = "default"
  }
  global_default = true
  value          = 100
  #preemption_policy = "Never"
}

resource "kubernetes_priority_class_v1" "important" {
  metadata {
    name = "important"
  }
  global_default = false
  value          = 1000
  #preemption_policy = "Never"
}
