resource "helm_release" "metallb" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_agent_token"] == "" ? 0 : 1
  name       = "metallb"
  chart      = "metallb"
  repository = "https://metallb.github.io/metallb"
  namespace  = "default"
  version    = var.versions.metallb.chart
  timeout    = "300"

  values = [<<EOF
controller:
  logLevel: warn
  priorityClassName: ${kubernetes_priority_class_v1.important.metadata.0.name}
  livenessprobe:
    enabled: true
    failurethreshold: 15
    initialdelayseconds: 10
    periodseconds: 10
    successthreshold: 1
    timeoutseconds: 2
  readinessprobe:
    enabled: true
    failurethreshold: 10
    initialdelayseconds: 20
    periodseconds: 10
    successthreshold: 1
    timeoutseconds: 2
speaker:
  logLevel: warn
  livenessProbe:
    enabled: true
    failureThreshold: 20
    initialDelaySeconds: 20
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 2
  readinessprobe:
    enabled: true
    failurethreshold: 20
    initialdelayseconds: 20
    periodseconds: 10
    successthreshold: 1
    timeoutseconds: 2
  startupprobe:
    enabled: true
    failurethreshold: 30
    periodseconds: 10
    successthreshold: 1
    timeoutseconds: 2
prometheus:
  scrapeAnnotations: true
  rbacPrometheus: false
  serviceMonitor:
    enabled: true
  prometheusRule:
    enabled: true
EOF
  ]
}

resource "kubernetes_manifest" "reserved_address_pool" {
  manifest = {
    "apiVersion" = "metallb.io/v1beta1"
    "kind"       = "IPAddressPool"
    "metadata" = {
      "name"      = "reserved-address-pool"
      "namespace" = "default"
    }
    "spec" = {
      "addresses"  = ["192.168.1.220-192.168.1.225"]
      "autoAssign" = false
    }
  }
  depends_on = [
    helm_release.metallb
  ]
}

resource "kubernetes_manifest" "default_address_pool" {
  manifest = {
    "apiVersion" = "metallb.io/v1beta1"
    "kind"       = "IPAddressPool"
    "metadata" = {
      "name"      = "default-address-pool"
      "namespace" = "default"
    }
    "spec" = {
      "addresses"  = ["192.168.1.226-192.168.1.235"]
      "autoAssign" = true
    }
  }
  depends_on = [
    helm_release.metallb
  ]
}

resource "kubernetes_manifest" "l2_advertisement" {
  manifest = {
    apiVersion = "metallb.io/v1beta1"
    kind       = "L2Advertisement"
    metadata = {
      name      = "default-l2-advertisement"
      namespace = "default"
    }
    #spec = {
    #  #IPAddressPools = [
    #  #  kubernetes_manifest.default_address_pool.manifest.metadata.name,
    #  #  kubernetes_manifest.reserved_address_pool.manifest.metadata.name
    #  #]
    #  #nodeSelectors = [
    #  #  { matchLabels = {
    #  #    "kubernetes.io/hostname" = "master"
    #  #  } }
    #  #]

    #}
  }
  depends_on = [
    helm_release.metallb
  ]
}
