#########################################################################
################################# ADGUARD ###############################
#########################################################################

#  tag: ${var.versions.adguard.image}

resource "kubernetes_config_map_v1" "adguard_config" {
  metadata {
    name = "adguard-config"
  }

  data = {
    "AdGuardHome.yaml" = <<EOF
http:
  pprof:
    enabled: false
dns:
  bind_hosts:
    - 0.0.0.0
  port: 53
  ratelimit: 0
  ratelimit_whitelist: []
  refuse_any: true
  upstream_dns:
    - https://dns10.quad9.net/dns-query
    - https://security.cloudflare-dns.com/dns-query
    - tls://security.cloudflare-dns.com
    - https://dns.adguard.com/dns-query
  #bootstrap_dns: ### This breaks after 0.137.36 for some reason. Fails to parse config
  #  - 8.8.8.8
  #  - 8.8.4.4
  all_servers: true
  fastest_addr: true
  cache_size: 150194304
  cache_ttl_min: 1
  cache_ttl_max: 84600
  cache_optimistic: true
  enable_dnssec: false
  max_goroutines: 300
  cache_time: 30
tls:
  enabled: false
filters:
  - enabled: true
    url: https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt
    name: AdGuard DNS filter
    id: 1
  - enabled: false
    url: https://adaway.org/hosts.txt
    name: AdAway
    id: 2
  - enabled: false
    url: https://www.malwaredomainlist.com/hostslist/hosts.txt
    name: MalwareDomainList.com Hosts List
    id: 4
whitelist_filters: []
user_rules: []
filtering:
  safebrowsing_block_host: standard-block.dns.adguard.com
  safebrowsing_cache_size: 1048576
  safesearch_cache_size: 1048576
  parental_cache_size: 1048576
  cache_time: 30
  filters_update_interval: 24
  blocked_response_ttl: 10
  filtering_enabled: true
  parental_enabled: false
  safebrowsing_enabled: false
  protection_enabled: true
  rewrites: 
    - domain: '*.${var.local_domain}'
      answer: ${var.ingress_ip}
schema_version: 27
EOF
  }
}

resource "kubernetes_deployment_v1" "adguard" {
  metadata {
    name = "adguard"
    labels = {
      app = "adguard"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "adguard"
      }
    }
    template {
      metadata {
        labels = {
          app = "adguard"
        }
      }
      spec {
        container {
          image   = "adguard/adguardhome:${var.versions.adguard.image}"
          name    = "example"
          command = ["sh", "-c"]
          args    = ["cp /adguard-config/AdGuardHome.yaml /opt/adguardhome/conf/AdGuardHome.yaml && /opt/adguardhome/AdGuardHome -c /opt/adguardhome/conf/AdGuardHome.yaml -w /opt/adguardhome/work"]

          resources {
            limits = {
              cpu    = "500m"
              memory = "300Mi"
            }
            requests = {
              cpu    = "300m"
              memory = "250Mi"
            }
          }
          liveness_probe {
            tcp_socket {
              port = 3000
            }
          }
          readiness_probe {
            tcp_socket {
              port = 3000
            }
          }
          volume_mount {
            mount_path = "/adguard-config"
            name       = "adguard-config"
          }
        }
        volume {
          name = "adguard-config"
          config_map {
            name         = kubernetes_config_map_v1.adguard_config.metadata.0.name
            default_mode = "0777"
          }
        }
      }
    }
  }
  timeouts {
    create = "1m"
    delete = "1m"
    update = "1m"
  }
}

resource "kubernetes_service" "adguard_udp" {
  metadata {
    name = "adguard-udp"
    annotations = {
      "metallb.universe.tf/loadBalancerIPs" : var.dns_server_ip
    }
  }
  spec {
    selector = {
      app = "adguard"
    }
    port {
      port        = 53
      target_port = 53
      protocol    = "UDP"
    }
    type = "LoadBalancer"
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_service" "adguard_management" {
  metadata {
    name = "adguard-management"
  }
  spec {
    selector = {
      app = "adguard"
    }
    port {
      port        = 3000
      target_port = 3000
    }
  }
}
