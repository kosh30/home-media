resource "kubernetes_endpoints_v1" "openwrt" {
  #count = var.create_ingress_for_nfs_server == true ? 1 : 0
  metadata {
    name = "openwrt"
    labels = {
      "kubernetes.io/service-name" = "openwrt"
      "name"                       = "openwrt"
    }
  }
  subset {
    address {
      ip = "192.168.1.1"
    }
    port {
      name     = "http"
      port     = 80
      protocol = "TCP"
    }
  }
}

resource "kubernetes_manifest" "openwrt_endpoint_slice" {
  #count = var.create_ingress_for_nfs_server == true ? 1 : 0
  manifest = {
    "apiVersion" = "discovery.k8s.io/v1"
    "kind"       = "EndpointSlice"
    "metadata" = {
      "name"      = "openwrt"
      "namespace" = "default"
      "labels" = {
        "name"                       = "openwrt"
        "kubernetes.io/service-name" = "openwrt"
      }
    }
    "addressType" = "IPv4"
    "ports" = [{
      "name"     = "http"
      "protocol" = "TCP"
      "port"     = "80"
    }]
    "endpoints" = [{
      "addresses" = [
        "192.168.1.1"
      ]
      "conditions" = {
        "ready" = "true"
      }
    }]
  }
}

resource "kubernetes_service_v1" "openwrt" {
  #count = var.create_ingress_for_nfs_server == true ? 1 : 0
  metadata {
    name = kubernetes_endpoints_v1.openwrt.metadata.0.name
    labels = {
      "name" = "openwrt"
    }
  }
  spec {
    port {
      port = 80
      name = "http"
    }
  }
}

resource "kubernetes_ingress_v1" "openwrt_internal" {
  #count = var.create_ingress_for_nfs_server == true ? 1 : 0
  metadata {
    name = "openwrt-internal"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      host = "openwrt.${var.local_domain}"
      http {
        path {
          backend {
            service {
              name = kubernetes_service_v1.openwrt.metadata.0.name
              port {
                number = 80
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}

#resource "kubernetes_ingress_v1" "qnap_external" {
#  count = var.create_ingress_for_nfs_server == true ? 1 : 0
#  metadata {
#    name = "qnap-external"
#    annotations = {
#      "kubernetes.io/ingress.class"    = "nginx"
#      "cert-manager.io/cluster-issuer" = kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
#      "nginx.org/redirect-to-https"    = true
#    }
#  }
#  spec {
#    tls {
#      secret_name = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
#      hosts       = ["qnap.${var.public_domain}"]
#    }
#    rule {
#      host = "qnap.${var.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = kubernetes_service_v1.qnap.0.metadata.0.name
#              port {
#                number = 8080
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#}
