<!-- BEGIN_TF_DOCS -->
This terraform module will set all the dependencies for the entire project and configure some infra related services.
The services installed and configured by this module are:
* [Longhorn](https://longhorn.io/)
* [HomeAssistant](https://www.home-assistant.io/)
* [AdGuard](https://adguard.com/en/welcome.html)
* [Cert-Manager](https://cert-manager.io/docs/)
* [File Browser](https://filebrowser.org/)
* [Homer](https://github.com/bastienwirtz/homer)
* [Gitlab Agent](https://docs.gitlab.com/ee/user/clusters/agent/) -- useful for setting up Gitlab CICD pipelines
* Traefik basic auth middleware

Besides installing and configuring these services, these other services will also be configured:
* Optionally: Set up your external DNS configs in Cloudflare (there are some requirements for this)
* Optionally: Set up [Cloudflare's Zero Trust](https://developers.cloudflare.com/cloudflare-one/) for the services you publish behind a [Cloudflare's Tunnel](https://www.cloudflare.com/products/tunnel/)
* Optionally: Set up ingress resources for your external NFS appliance like a qnap (in case you have one, hence the "optional")

This terraform root module should be deployed first, since this module sets up services that are required by the other modules. Also,
other modules will read the outputs from this module using [terraform\_remote\_state Data Sources](https://developer.hashicorp.com/terraform/language/state/remote-state-data).

This module might fail due to timeouts depending on your infrastructure. In that case, you should be fine by running it again.

## Requirements:

### External DNS and cloudflare

If setting up your external DNS (only cloudflare is supported in this repo), you need a Cloudflare account with your domain already added (and Cloudflare also needs to be the authoritative DNS for your domain).
Also, a cloudflare API key is required to configure the cloudflare provider.

*NOTE:* This is only optional, you can also configure your external DNS records (using [DuckDNS](https://www.duckdns.org/) for free for example. In that case, set your domain in *public\_domain* (duckdns.org for example) and set
*configure\_external\_dns* to false)

### Secrets
Sensitive information is encrypted. You need to fill out a few settings this way. To do so, configure secrets-decrypted-sample:

```
{
  "cloudflare_api_key": "",
  "google_cloudflare_oauth_client_id": "",
  "google_cloudflare_oauth_client_secret": "",
  "gitlab_kubernetes_agent_token": "",
  "prowlarr_api_key": "",
  "radarr_api_key": "",
  "sonarr_api_key": "",
  "traefik_basic_auth_user": "",
  "traefik_basic_auth_password": ""
}
```

Radarr, Sonarr and Prowlarr API keys can be left empty for the moment. You can fill those out after those services are installed and configured (they are installed by a different terraform root module). <br/>
Those API keys are only used to set up the integration with Homer, so they are not even important. <br/>
**gitlab\_kubernetes\_agent\_token** contains a token to register your cluster in your gitlab project. If this token is left empty ("") then the agent will not be installed. <br/>
To generate this token follow [these instructions](https://docs.gitlab.com/ee/user/clusters/agent/install/#register-the-agent-with-gitlab) <br/>
**cludflare\_api\_key** is used to set up your external DNS records and argo tunnels. It can be left empty if configure\_external\_dns is set to false <br/>
**google\_cloudflare\_oauth\_client\_id** and **google\_cloudflare\_oauth\_client\_secret** Besides using an ingress and using an A record to point to your cluster's public IP, [Cloudflare tunnels](https://www.cloudflare.com/products/tunnel/) can be used for free. On top of that, Cloudflare Zero trust is used to secure your apps using [Cloudflare Applications](https://developers.cloudflare.com/cloudflare-one/applications/) and a [Google identity provider](https://developers.cloudflare.com/cloudflare-one/identity/idp-integration/google/) is used to grant access to your apps. These vars define the ID and secret from your Google OAuth configuration for this client (more info in Cloudflare's docs) <br/>
**traefik\_basic\_auth\_user** and **traefik\_basic\_auth\_password** Some endpoints will require basic auth (configured through traefik). These are the credentials

To encrypt this file (assuming you installed sops and generated a key file as explained [here](https://gitlab.com/sergiojvg/home-media/-/tree/main/terraform/infra#secrets)), run:

```
sops --encrypt --age $YOUR_SOPS_PUBLIC_KEY secrets-decrypted-sample > secrets.json
```
If secrets.json is missing, this module will fail

### Terraform variables

Configure the terraform variables described in this module. *terraform.tfvars* is a good place to do so

## Debugging
Longhorn can take a lot of time to be fully installed. The helm chart will report it as ready before all the pods are running. To check the status, run:

```
kubectl get pod -n longhorn-system
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 3.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | 3.4.3 |
| <a name="requirement_sops"></a> [sops](#requirement\_sops) | ~> 0.5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | 3.32.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.8.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.16.1 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.4.3 |
| <a name="provider_sops"></a> [sops](#provider\_sops) | 0.7.2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_access_application.access_application](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_application) | resource |
| [cloudflare_access_group.home_emails](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_group) | resource |
| [cloudflare_access_identity_provider.google_oauth](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_identity_provider) | resource |
| [cloudflare_access_policy.access_policy](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_policy) | resource |
| [cloudflare_api_token.letsencrypt_dns_challenge](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/api_token) | resource |
| [cloudflare_argo_tunnel.ben](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/argo_tunnel) | resource |
| [cloudflare_record.dns_a_record](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_record.homer_argo_tunnel](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [helm_release.adguard](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.cert-manager](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.filebrowser](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.gitlab_agent](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.homeassistant](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.homer](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.longhorn](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_config_map_v1.cloudflared_config](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map_v1) | resource |
| [kubernetes_deployment.cloudflared](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/deployment) | resource |
| [kubernetes_endpoints_v1.qnap](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/endpoints_v1) | resource |
| [kubernetes_ingress_v1.homeassistant-vscode](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_ingress_v1.homeassistant_external](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_ingress_v1.homeassistant_external_http_redirect](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_ingress_v1.homeassistant_home](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_ingress_v1.qnap_external](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_ingress_v1.qnap_external_http_redirect](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_ingress_v1.qnap_internal](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress_v1) | resource |
| [kubernetes_manifest.dns_clusterissuer](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_manifest.qnap_endpoint_slice](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_manifest.traefik_redirect_middleware](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_manifest.wildcard_certificate](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_namespace.longhorn](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_persistent_volume.multimedia](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/persistent_volume) | resource |
| [kubernetes_persistent_volume_claim.multimedia](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/persistent_volume_claim) | resource |
| [kubernetes_priority_class_v1.default](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/priority_class_v1) | resource |
| [kubernetes_priority_class_v1.important](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/priority_class_v1) | resource |
| [kubernetes_secret.cloudflare_token_letsencrypt](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.tunnel_credentials](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_service_v1.qnap](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_v1) | resource |
| [kubernetes_storage_class.nfs_dummy](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [random_id.argo_secret](https://registry.terraform.io/providers/hashicorp/random/3.4.3/docs/resources/id) | resource |
| [cloudflare_api_token_permission_groups.all](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/api_token_permission_groups) | data source |
| [cloudflare_zone.vegabookholt](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |
| [sops_file.secrets](https://registry.terraform.io/providers/carlpett/sops/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudflare_app_tunnels"></a> [cloudflare\_app\_tunnels](#input\_cloudflare\_app\_tunnels) | List of maps containing the services that will be routed through Cloudflare's Argo Tunnel (instead of the cluster's ingress containing a Public IP). Each map requires the keys 'hostname' and 'kubernetes\_service\_url | `list(map(string))` | <pre>[<br>  {<br>    "hostname": "adguard",<br>    "kubernetes_service_url": "http://adguard-adguard-home:3000"<br>  }<br>]</pre> | no |
| <a name="input_configure_external_dns"></a> [configure\_external\_dns](#input\_configure\_external\_dns) | Configures your DNS entries using cloudflare as provider. A cloudflare account with your domain registered on it is required | `bool` | `false` | no |
| <a name="input_create_ingress_for_nfs_server"></a> [create\_ingress\_for\_nfs\_server](#input\_create\_ingress\_for\_nfs\_server) | Creates ingress resources for your external storage appliance. This is useful if you use a qnap or synology device | `bool` | `true` | no |
| <a name="input_dns_hostnames"></a> [dns\_hostnames](#input\_dns\_hostnames) | List of hostnames to be configured as A records in Cloudflare DNS. These DNS records will be pointing to the cluster's public IP | `list(string)` | `[]` | no |
| <a name="input_ingress_nodes"></a> [ingress\_nodes](#input\_ingress\_nodes) | List of nodes in the cluster publishing the ingress controller (external IPs under 'kubectl get service -n kube-system traefik'). These are the ips that will be advertised by the DNS (adguard) for the ingresses in this cluster | `list(any)` | `[]` | no |
| <a name="input_local_domain"></a> [local\_domain](#input\_local\_domain) | TLDs used in local network. Ex: my.house | `string` | n/a | yes |
| <a name="input_media_size"></a> [media\_size](#input\_media\_size) | Size for the PVC that will store movies. Should be equal or smaller than the size of the NFS export | `string` | n/a | yes |
| <a name="input_nfs_server"></a> [nfs\_server](#input\_nfs\_server) | NFS server IP used for the nfs SC | `string` | n/a | yes |
| <a name="input_personal_email"></a> [personal\_email](#input\_personal\_email) | Email used for registrations and letsencrypt ssl certs | `string` | `"nobody@gmail.com"` | no |
| <a name="input_public_domain"></a> [public\_domain](#input\_public\_domain) | TLDs used for public access. Ex: myhouse.ga | `string` | n/a | yes |
| <a name="input_public_ip"></a> [public\_ip](#input\_public\_ip) | Public IP from the ISP. This IP requires a prot forward to ports 80 and 443 and will be associated with A records created under public\_domain | `string` | `"199.199.199.199"` | no |
| <a name="input_timezone"></a> [timezone](#input\_timezone) | Timezone in this format: https://www.php.net/manual/en/timezones.php | `string` | n/a | yes |
| <a name="input_versions"></a> [versions](#input\_versions) | n/a | <pre>object({<br>    homeassistant = object({<br>      chart = string<br>      image = string<br>    })<br>    adguard = object({<br>      chart = string<br>      image = string<br>    })<br>    homer = object({<br>      chart = string<br>      image = string<br>    })<br>    filebrowser = object({<br>      chart = string<br>    })<br>    certmanager = object({<br>      chart = string<br>    })<br>    cloudflared = object({<br>      image = string<br>    })<br>  })</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_certmanager_clusterissuer_name"></a> [certmanager\_clusterissuer\_name](#output\_certmanager\_clusterissuer\_name) | Name of the cluster issuer resource used to generate SSL certificates |
| <a name="output_important_priority_class"></a> [important\_priority\_class](#output\_important\_priority\_class) | Name of the priority class to use for important services. Recommended for services with a high resources request so it can evict other pods |
| <a name="output_local_domain"></a> [local\_domain](#output\_local\_domain) | Local domain used for the services in this cluster (only accessible from the LAN (because private IPs are used) and it is only resolved by Adguard) |
| <a name="output_longhorn_storage_class_name"></a> [longhorn\_storage\_class\_name](#output\_longhorn\_storage\_class\_name) | Name of the storage class created by Longhorn (the SDS we use) |
| <a name="output_nfs_multimedia_pvc"></a> [nfs\_multimedia\_pvc](#output\_nfs\_multimedia\_pvc) | Name of the PVC pointing to the NFS drive |
| <a name="output_nfs_multimedia_storage_class"></a> [nfs\_multimedia\_storage\_class](#output\_nfs\_multimedia\_storage\_class) | Name of the storage class used by the NFS (multimedia) PV and PVC |
| <a name="output_public_domain"></a> [public\_domain](#output\_public\_domain) | Public domain used for services in this cluster |
| <a name="output_public_ip"></a> [public\_ip](#output\_public\_ip) | Public IP used for this cluster |
| <a name="output_timezone"></a> [timezone](#output\_timezone) | Timezone in this format: https://www.php.net/manual/en/timezones.php |
| <a name="output_traefik_redirect_middleware_name"></a> [traefik\_redirect\_middleware\_name](#output\_traefik\_redirect\_middleware\_name) | Name of the cluster issuer resource used to generate SSL certificates |
| <a name="output_wildcard_ssl_certificate_secret_name"></a> [wildcard\_ssl\_certificate\_secret\_name](#output\_wildcard\_ssl\_certificate\_secret\_name) | Name of the secret hosting the wildcard SSL certificate |
<!-- END_TF_DOCS -->
