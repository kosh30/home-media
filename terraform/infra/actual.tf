#########################################################################
################################ ACTUAL #################################
#########################################################################

resource "kubernetes_persistent_volume" "actual_configs" {
  metadata {
    name = "actual-configs"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "5Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata.0.name
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.actual.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim" "actual_configs" {
  metadata {
    name = "actual"
  }
  spec {
    volume_name        = kubernetes_persistent_volume.actual_configs.metadata.0.name
    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata.0.name
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_deployment_v1" "actual" {
  metadata {
    name = "actual"
    labels = {
      "app.kubernetes.io/name" = "actual"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        "app.kubernetes.io/name" = "actual"
      }
    }
    template {
      metadata {
        labels = {
          "app.kubernetes.io/name" = "actual"
        }
      }
      spec {
        container {
          image = "ghcr.io/actualbudget/actual-server:sha-7865e08-alpine@sha256:c4319ec922ecc0c7ec6a1458aea5dd1ca23b97ce26c851482ed76e7e2e509343"
          name  = "actual"
          #command = ["sh", "-c"]
          #args    = ["cp /adguard-config/AdGuardHome.yaml /opt/adguardhome/conf/AdGuardHome.yaml && /opt/adguardhome/AdGuardHome -c /opt/adguardhome/conf/AdGuardHome.yaml -w /opt/adguardhome/work"]

          resources {
            limits = {
              cpu    = "400m"
              memory = "400Mi"
            }
            requests = {
              cpu    = "200m"
              memory = "250Mi"
            }
          }
          liveness_probe {
            http_get {
              path = "/"
              port = 5006
            }
            initial_delay_seconds = 20
            period_seconds        = 5
            failure_threshold     = 20
          }
          readiness_probe {
            http_get {
              path = "/"
              port = 5006
            }
            period_seconds    = 2
            failure_threshold = 2
            success_threshold = 1
          }
          volume_mount {
            mount_path = "/data"
            name       = "actual-data"
          }
        }
        volume {
          name = "actual-data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.actual_configs.metadata.0.name
          }
        }
      }
    }
  }
  timeouts {
    create = "1m"
    delete = "1m"
    update = "1m"
  }
}

resource "kubernetes_service" "actual" {
  metadata {
    name = "actual"
  }
  spec {
    selector = {
      "app.kubernetes.io/name" = "actual"
    }
    port {
      port        = 5006
      target_port = 5006
    }
  }
}


#resource "helm_release" "filebrowser" {
#  name       = "actual"
#  chart      = "actual"
#  repository = "https://beluga-cloud.github.io/charts"
#  namespace  = "default"
#  timeout    = "100"
#  version    = var.versions.filebrowser.chart
#
#  values = [<<EOF
#resources:
#  limits:
#    cpu: 200m
#    memory: 200Mi
#  requests:
#    cpu: 150m
#    memory: 150Mi
#ingress:
#  main:
#    enabled: true
#    annotations: 
#      kubernetes.io/ingress.class: nginx
#    hosts:
#      - host: files.${var.local_domain}
#        paths:
#          - path: /
#config: |
#  {
#    "port": 80,
#    "baseURL": "",
#    "address": "",
#    "log": "stdout",
#    "database": "/config/database.db",
#    "root": "/downloads"
#  }
#persistence:
#  config:
#    enabled: false
#    mountpath: /config
#  data:
#    enabled: true
#    emptyDir: false
#    mountPath: /downloads
#    storageClass: ${kubernetes_storage_class.nfs_dummy.metadata.0.name}
#    accessMode: ReadWriteMany
#    existingClaim: ${kubernetes_persistent_volume_claim.multimedia.metadata.0.name}
#EOF
#  ]
#}
#
