resource "helm_release" "gitlab_agent" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_agent_token"] == "" ? 0 : 1
  name       = "gitlab-agent"
  chart      = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  namespace  = "default"
  version    = "1.9.1"
  timeout    = "300"

  values = [<<EOF
config:
  kasAddress: wss://kas.gitlab.com
  token: ${data.sops_file.secrets.data["gitlab_kubernetes_agent_token"]}
resources:
  limits:
   cpu: 800m
   memory: 200Mi
  requests:
   cpu: 200m
   memory: 100Mi
EOF
  ]
}
