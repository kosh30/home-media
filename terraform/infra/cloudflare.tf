#resource "cloudflare_zone" "vegabookholt" {
#  zone       = var.public_domain
#  plan       = "free"
#  jump_start = false
#}

data "cloudflare_zone" "vegabookholt" {
  count = var.configure_external_dns == true ? 1 : 0
  name  = var.public_domain
}

data "cloudflare_api_token_permission_groups" "all" {
  count = var.configure_external_dns == true ? 1 : 0
}

resource "cloudflare_api_token" "letsencrypt_dns_challenge" {
  count = var.configure_external_dns == true ? 1 : 0
  name  = "letsencrypt-dns-challenge"

  policy {
    permission_groups = [
      data.cloudflare_api_token_permission_groups.all.0.permissions["DNS Write"],
      data.cloudflare_api_token_permission_groups.all.0.permissions["SSL and Certificates Write"],
    ]
    resources = {
      "com.cloudflare.api.account.zone.${data.cloudflare_zone.vegabookholt.0.id}" = "*"
    }
  }
}

resource "kubernetes_secret" "cloudflare_token_letsencrypt" {
  count = var.configure_external_dns == true ? 1 : 0
  metadata {
    name = "cloudflare-token-letsencrypt"
  }

  data = {
    api-key = cloudflare_api_token.letsencrypt_dns_challenge.0.value
  }
}

#resource "cloudflare_record" "dns_a_records" {
#  zone_id = data.cloudflare_zone.vegabookholt.0.id
#  name    = "*"
#  value   = var.public_ip
#  type    = "A"
#  ttl     = 60
#}

resource "cloudflare_record" "dns_a_record" {
  count   = length(var.dns_hostnames)
  zone_id = data.cloudflare_zone.vegabookholt.0.id
  name    = var.dns_hostnames[count.index]
  value   = var.public_ip
  type    = "A"
  ttl     = 60
}
