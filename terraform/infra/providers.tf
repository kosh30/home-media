terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/34632632/terraform/state/infra"
    lock_address   = "https://gitlab.com/api/v4/projects/34632632/terraform/state/infra/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/34632632/terraform/state/infra/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    sops = {
      source  = "carlpett/sops"
      version = "~> 0.5"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.4"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.25"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.12"
    }
  }
}

provider "kubernetes" {
  ## Set KUBE_CONFIG_PATH env var
  #config_path = local.k3s_config_file_path
}

provider "helm" {
  kubernetes {
    ## Set KUBE_CONFIG_PATH env var
    #config_path = local.k3s_config_file_path
  }
}

provider "cloudflare" {
  email   = var.personal_email
  api_key = data.sops_file.secrets.data["cloudflare_api_key"]
}

provider "random" {}
