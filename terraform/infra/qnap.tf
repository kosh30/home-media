resource "kubernetes_endpoints_v1" "qnap" {
  count = var.create_ingress_for_nfs_server == true ? 1 : 0
  metadata {
    name = "qnap"
    labels = {
      "kubernetes.io/service-name" = "qnap"
      "name"                       = "qnap"
    }
  }
  subset {
    address {
      ip = var.nfs_server
    }
    port {
      name     = "http"
      port     = 8080
      protocol = "TCP"
    }
  }
}

resource "kubernetes_manifest" "qnap_endpoint_slice" {
  count = var.create_ingress_for_nfs_server == true ? 1 : 0
  manifest = {
    "apiVersion" = "discovery.k8s.io/v1"
    "kind"       = "EndpointSlice"
    "metadata" = {
      "name"      = "qnap"
      "namespace" = "default"
      "labels" = {
        "name"                       = "qnap"
        "kubernetes.io/service-name" = "qnap"
      }
    }
    "addressType" = "IPv4"
    "ports" = [{
      "name"     = "http"
      "protocol" = "TCP"
      "port"     = "8080"
    }]
    "endpoints" = [{
      "addresses" = [
        var.nfs_server
      ]
      "conditions" = {
        "ready" = "true"
      }
    }]
  }
}

resource "kubernetes_service_v1" "qnap" {
  count = var.create_ingress_for_nfs_server == true ? 1 : 0
  metadata {
    name = kubernetes_endpoints_v1.qnap.0.metadata.0.name
    labels = {
      "name" = "qnap"
    }
  }
  spec {
    port {
      port = 8080
      name = "http"
    }
  }
}

resource "kubernetes_ingress_v1" "qnap_internal" {
  count = var.create_ingress_for_nfs_server == true ? 1 : 0
  metadata {
    name = "qnap-internal"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      host = "qnap.${var.local_domain}"
      http {
        path {
          backend {
            service {
              name = kubernetes_service_v1.qnap.0.metadata.0.name
              port {
                number = 8080
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}

#resource "kubernetes_ingress_v1" "qnap_external" {
#  count = var.create_ingress_for_nfs_server == true ? 1 : 0
#  metadata {
#    name = "qnap-external"
#    annotations = {
#      "kubernetes.io/ingress.class"    = "nginx"
#      "cert-manager.io/cluster-issuer" = kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
#      "nginx.org/redirect-to-https"    = true
#    }
#  }
#  spec {
#    tls {
#      secret_name = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
#      hosts       = ["qnap.${var.public_domain}"]
#    }
#    rule {
#      host = "qnap.${var.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = kubernetes_service_v1.qnap.0.metadata.0.name
#              port {
#                number = 8080
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#}
