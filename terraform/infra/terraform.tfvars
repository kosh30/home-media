nfs_server     = "192.168.1.205"
timezone       = "Europe/Amsterdam"
local_domain   = "my.house"
public_domain  = "vegabookholt.uk"
personal_email = "sergiojvg92@gmail.com"
#public_ip                     = "85.144.130.41"
public_ip                     = "85.145.78.147"
media_size                    = "2500Gi"
create_ingress_for_nfs_server = true
configure_external_dns        = true
dns_hostnames                 = ["homeassistant", "plex", "jellyfin", "homer2", "grafana2"]
dns_server_ip                 = "192.168.1.220"
ingress_ip                    = "192.168.1.201"
#cloudflare_tunnel_hostnames = ["homer", "homer23", "homer24", "homer25"]
cloudflare_app_tunnels = [
  {
    hostname               = "homer"
    kubernetes_service_url = "http://homer:8080"
  },
  {
    hostname               = "prometheus"
    kubernetes_service_url = "http://prometheus-server:80"
  },
  {
    hostname               = "adguard"
    kubernetes_service_url = "http://adguard-management:3000"
  },
  {
    hostname               = "radarr"
    kubernetes_service_url = "http://radarr:7878"
  },
  {
    hostname               = "sonarr"
    kubernetes_service_url = "http://sonarr:8989"
  },
  {
    hostname               = "bazarr"
    kubernetes_service_url = "http://bazarr:6767"
  },
  {
    hostname               = "prowlarr"
    kubernetes_service_url = "http://prowlarr:9696"
  },
  {
    hostname               = "alertmanager"
    kubernetes_service_url = "http://prometheus-alertmanager:9093"
  },
  {
    hostname               = "blackbox-exporter"
    kubernetes_service_url = "http://prometheus-blackbox-exporter:9115"
  },
  {
    hostname               = "code"
    kubernetes_service_url = "http://homeassistant-home-assistant-codeserver:12321"
  },
  {
    hostname               = "files"
    kubernetes_service_url = "http://filebrowser:80"
  },
  {
    hostname               = "grafana"
    kubernetes_service_url = "http://grafana:80"
  },
  {
    hostname               = "transmission"
    kubernetes_service_url = "http://transmission:9091"
  },
  {
    hostname               = "mealie"
    kubernetes_service_url = "http://mealie:9000"
  },
  {
    hostname               = "qnap"
    kubernetes_service_url = "http://qnap:8080"
  },
  {
    hostname               = "overseerr"
    kubernetes_service_url = "http://overseerr:5055"
  },
  {
    hostname               = "homeassistant2"
    kubernetes_service_url = "http://homeassistant-home-assistant:8123"
  },
  {
    hostname               = "fileflows"
    kubernetes_service_url = "http://fileflows:5000"
  },
  {
    hostname               = "actual"
    kubernetes_service_url = "http://actual:5006"
  },
]

ingress_nodes = ["192.168.1.201"]

versions = {
  homeassistant = {
    chart = "13.4.2"
    image = "2024.2.0" ### https://hub.docker.com/r/homeassistant/home-assistant/tags
  }
  adguard = {
    image = "v0.107.43" #https://hub.docker.com/r/adguard/adguardhome/tags
  }
  homer = {
    chart = "8.0.2"
    image = "v23.10.1" # https://hub.docker.com/r/b4bz/homer/tags
  }
  filebrowser = {
    chart = "1.4.2"
  }
  certmanager = {
    chart = "1.13.3" # jetstack --> cert-manager
  }
  cloudflared = {
    image = "2024.1.4" #https://hub.docker.com/r/cloudflare/cloudflared/tags
  }
  metallb = {
    chart = "0.13.12" # metallb --> metallb
  }
  nginx = {
    chart = "1.0.2" # nginx-stable --> nginx-ingress
  }
}
