variable "versions" {
  type = object({
    homeassistant = object({
      chart = string
      image = string
    })
    adguard = object({
      image = string
    })
    homer = object({
      chart = string
      image = string
    })
    filebrowser = object({
      chart = string
    })
    certmanager = object({
      chart = string
    })
    cloudflared = object({
      image = string
    })
    metallb = object({
      chart = string
    })
    nginx = object({
      chart = string
    })
  })
}

variable "ingress_nodes" {
  type        = list(any)
  default     = []
  description = "List of nodes in the cluster publishing the ingress controller (external IPs under 'kubectl get service -n kube-system traefik'). These are the ips that will be advertised by the DNS (adguard) for the ingresses in this cluster"
}

variable "nfs_server" {
  type        = string
  description = "NFS server IP used for the nfs SC"
}

variable "create_ingress_for_nfs_server" {
  type        = bool
  default     = true
  description = "Creates ingress resources for your external storage appliance. This is useful if you use a qnap or synology device"
}

variable "media_size" {
  type        = string
  description = "Size for the PVC that will store movies. Should be equal or smaller than the size of the NFS export"
}

variable "timezone" {
  type        = string
  description = "Timezone in this format: https://www.php.net/manual/en/timezones.php"
}

variable "local_domain" {
  type        = string
  description = "TLDs used in local network. Ex: my.house"
}

variable "public_ip" {
  type        = string
  description = "Public IP from the ISP. This IP requires a prot forward to ports 80 and 443 and will be associated with A records created under public_domain"
  default     = "199.199.199.199"
}

variable "public_domain" {
  type        = string
  description = "TLDs used for public access. Ex: myhouse.ga"
}

variable "configure_external_dns" {
  type        = bool
  default     = false
  description = "Configures your DNS entries using cloudflare as provider. A cloudflare account with your domain registered on it is required"
}

variable "personal_email" {
  type        = string
  description = "Email used for registrations and letsencrypt ssl certs"
  default     = "nobody@gmail.com"
}

variable "dns_hostnames" {
  type        = list(string)
  description = "List of hostnames to be configured as A records in Cloudflare DNS. These DNS records will be pointing to the cluster's public IP"
  default     = []
}

variable "dns_server_ip" {
  type        = string
  description = "IP from the DNS server"
  default     = "192.168.1.220"
}

variable "ingress_ip" {
  type        = string
  description = "IP from the Ingress controller"
  default     = "192.168.1.221"
}

variable "cloudflare_app_tunnels" {
  type        = list(map(string))
  description = "List of maps containing the services that will be routed through Cloudflare's Argo Tunnel (instead of the cluster's ingress containing a Public IP). Each map requires the keys 'hostname' and 'kubernetes_service_url"
  default = [
    {
      hostname               = "adguard"
      kubernetes_service_url = "http://adguard-adguard-home:3000"
    },
  ]
}
