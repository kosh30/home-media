#########################################################################
################################ FILEBROWSER ############################
#########################################################################
resource "helm_release" "filebrowser" {
  name       = "filebrowser"
  chart      = "filebrowser"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "100"
  version    = var.versions.filebrowser.chart

  values = [<<EOF
resources:
  limits:
    cpu: 200m
    memory: 200Mi
  requests:
    cpu: 150m
    memory: 150Mi
ingress:
  main:
    enabled: true
    annotations: 
      kubernetes.io/ingress.class: nginx
    hosts:
      - host: files.${var.local_domain}
        paths:
          - path: /
config: |
  {
    "port": 80,
    "baseURL": "",
    "address": "",
    "log": "stdout",
    "database": "/config/database.db",
    "root": "/downloads"
  }
persistence:
  config:
    enabled: false
    mountpath: /config
  data:
    enabled: true
    emptyDir: false
    mountPath: /downloads
    storageClass: ${kubernetes_storage_class.nfs_dummy.metadata.0.name}
    accessMode: ReadWriteMany
    existingClaim: ${kubernetes_persistent_volume_claim.multimedia.metadata.0.name}
EOF
  ]
}
