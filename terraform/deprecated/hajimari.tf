##########################################################################
################################## HOMER #################################
##########################################################################
resource "helm_release" "hajimari" {
  name       = "hajimari"
  chart      = "hajimari"
  repository = "https://hajimari.io"
  namespace  = "default"
  timeout    = "250"
  #version    = var.versions.homer.chart
  version = "2.0.2"

  values = [<<EOF
# @default -- See below
hajimari:
  # -- The name of this instance, this allows running multiple 
  # instances of Hajimari on the same cluster
  instanceName: null

  # -- Set to true to show all discovered applications by default.
  defaultEnable: false

  # -- Namespace selector to use for discovering applications
  namespaceSelector:
    matchNames:
resources:
  limits:
    cpu: 120m
    memory: 100Mi
  requests:
    cpu: 80m
    memory: 100Mi
persistence:
  data:
    enabled: false
ingress:
  main:
    enabled: true
    hosts:
      - host: hajimari.${var.local_domain}
        paths:
          - path: /
EOF
  ]
}
