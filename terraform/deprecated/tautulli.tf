##########################################################################
############################### TAUTULLI #################################
##########################################################################
#
#resource "kubernetes_config_map" "tautulli_config" {
#  metadata {
#    name = "tautulli-config"
#  }
#
#  data = {
#    "config.ini" = "${local.tautulli_config}"
#  }
#}
#
#resource "helm_release" "tautulli" {
#  name       = "tautulli"
#  chart      = "tautulli"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "200"
#  version    = "11.4.2"
#
#  values = [<<EOF
#initContainers:
#  copy-configmap:
#    name: copy-configmap
#    image: busybox
#    command:
#    - "sh"
#    - "-c"
#    - |
#      echo "Copying tautulli config file..."
#      cp /tmp/config/config.ini /opt/config/config.ini
#      echo "--> Current config:"
#      cat /opt/config/config.ini
#      chmod -R 777 /opt/config
#    volumeMounts:
#    - name: configfile
#      mountPath: /tmp/config
#    - name: config
#      mountPath: /opt/config
#    securityContext:
#      runAsUser: 0
##initContainers:
##  update-volume-permission:
##    image: busybox
##    command: ["sh", "-c", "chmod -R 777 /config"]
##    volumeMounts:
##    - name: config
##      mountPath: /config
##    securityContext:
##      runAsUser: 0
#resources:
#  limits:
#    cpu: 250m
#    memory: 150Mi
#  requests:
#    cpu: 200m
#    memory: 150Mi
#persistence:
#  configfile:
#    enabled: true
#    type: custom
#    mountPath: /tmp/config
#    volumeSpec:
#      configMap:
#        name: ${kubernetes_config_map.tautulli_config.metadata.0.name}
#        defaultMode: 0555
#  config:
#    enabled: true
#    type: emptyDir
#    accessMode: ReadWriteOnce
#    size: 300Mi
#probes:
#  startup:
#    spec:
#      initialDelaySeconds: 0
#      timeoutSeconds: 3
#      periodSeconds: 10
#      failureThreshold: 50
#ingress:
#  main:
#    enabled: true
#    hosts:
#      - host: tautulli.${data.terraform_remote_state.infra.outputs.local_domain}
#        paths:
#          - path: /
#EOF
#  ]
#}
