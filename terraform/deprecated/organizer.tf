##########################################################################
################################## ORGANIZER #############################
##########################################################################
resource "helm_release" "organizr" {
  name       = "organizr"
  chart      = "organizr"
  repository = "https://k8s-at-home.com/charts/"
  namespace  = "default"
  timeout    = "450"
  version    = "7.4.2"

  values = [<<EOF
#image:
#  repository: b4bz/homer
#  tag: ${var.versions.homer.image}
#resources:
#  limits:
#    cpu: 80m
#    memory: 50Mi
#  requests:
#    cpu: 80m
#    memory: 50Mi
ingress:
  main:
    enabled: true
    hosts:
      - host: organizr.${var.domain}
        paths:
          - path: /
EOF
  ]
}
