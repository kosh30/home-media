resource "kubernetes_persistent_volume" "nextcloud" {
  metadata {
    name = "nextcloud"
  }
  spec {
    capacity = {
      #storage = var.media_size
      storage = "100Gi"
    }
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = kubernetes_storage_class.nfs-dummy.metadata[0].name
    persistent_volume_source {
      iscsi {
        fs_type       = "ext4"
        iqn           = "iqn.2004-04.com.qnap:ts-219pii:iscsi.nas.c346ec"
        target_portal = "192.168.1.205"
        #lun = 0
      }
    }
  }
  timeouts {
    create = "1m"
  }
}

resource "kubernetes_persistent_volume_claim" "nextcloud" {
  metadata {
    name = "nextcloud"
  }
  spec {
    volume_name        = kubernetes_persistent_volume.nextcloud.metadata.0.name
    storage_class_name = kubernetes_storage_class.nfs-dummy.metadata[0].name
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "100Gi"
      }
    }
  }
  timeouts {
    create = "1m"
  }
}
###########################################################################
################################### PLEX ##################################
###########################################################################
#resource "helm_release" "nextcloud" {
#  name       = "nextcloud"
#  chart      = "nextcloud"
#  repository = "https://nextcloud.github.io/helm/"
#  namespace  = "default"
#  timeout    = "300"
#  version    = "3.3.6"
#
#  values = [<<EOF
#ingress:
#  enabled: true
#  #tls:
#  #  - secretName: ${data.terraform_remote_state.infra.outputs.wildcard_ssl_certificate_secret_name}
#  #    hosts:
#  #      - nextcloud.${var.public_domain}
#  #annotations:
#  #  kubernetes.io/ingress.class: traefik
#  #  cert-manager.io/cluster-issuer: ${data.terraform_remote_state.infra.outputs.certmanager_clusterissuer_name}
#  #  traefik.ingress.kubernetes.io/router.tls: 'true'
#  #  traefik.ingress.kubernetes.io/router.entrypoints: websecure
#nextcloud:
#  host: nextcloud.${var.public_domain}
#  username: kernel
#  password: ${data.sops_file.secrets.data["nextcloud_password"]}
#livenessProbe:
#  enabled: true
#  initialDelaySeconds: 10
#  periodSeconds: 10
#  timeoutSeconds: 5
#  failureThreshold: 3
#  successThreshold: 1
#readinessProbe:
#  enabled: true
#  initialDelaySeconds: 10
#  periodSeconds: 10
#  timeoutSeconds: 5
#  failureThreshold: 3
#  successThreshold: 1
#startupProbe:
#  enabled: true
#  initialDelaySeconds: 30
#  periodSeconds: 30
#  timeoutSeconds: 5
#  failureThreshold: 120
#  successThreshold: 1
#resources:
#  limits:
#   cpu: 900m
#   memory: 800Mi
#  requests:
#   cpu: 700m
#   memory: 700Mi
#persistence:
#  enabled: true
#  #storageClass: "longhorn"
#  #size: 1Gi
#  existingClaim: ${kubernetes_persistent_volume_claim.nextcloud.metadata.0.name}
#  accessMode: ReadWriteOnce
#  ##size: 8Gi
#  #### Use an additional pvc for the data directory rather than a subpath of the default PVC
#  #### Useful to store data on a different storageClass (e.g. on slower disks)
#  nextcloudData:
#    enabled: false
#    #subPath: "."
#    #storageClass: "${local.sc_name}"
#    #existingClaim: ${kubernetes_persistent_volume_claim.nextcloud.metadata.0.name}
#    #accessMode: ReadWriteOnce
#    #size: 8Gi
#EOF
#  ]
#}
