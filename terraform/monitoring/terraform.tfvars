versions = {
  grafana = {
    chart = "7.2.4" ### grafana/grafana
  }
  prometheus = {
    chart = "25.10.0" ### prometheus-community/prometheus
  }
  prometheus_blackbox_exporter = {
    chart = "8.10.0" ### prometheus-community/prometheus-blackbox-exporter
  }
  promtail = {
    chart = "6.15.4" ### grafana/promtail
  }
  otel_collector = {
    chart = "0.80.0" ### open-telemetry/opentelemetry-collector
  }
  node_problem_detector = {
    chart = "2.3.12" ### deliveryhero/node-problem-detector
  }
}
