terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/34632632/terraform/state/monitoring"
    lock_address   = "https://gitlab.com/api/v4/projects/34632632/terraform/state/monitoring/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/34632632/terraform/state/monitoring/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    sops = {
      source  = "carlpett/sops"
      version = "~> 0.5"
    }
    grafana = {
      source  = "grafana/grafana"
      version = " ~> 2.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.25"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.12"
    }
  }
}

provider "kubernetes" {
  ## Set KUBE_CONFIG_PATH env var
  #config_path = local.k3s_config_file_path
}

provider "helm" {
  kubernetes {
    ## Set KUBE_CONFIG_PATH env var
    #config_path = local.k3s_config_file_path
  }
}

provider "grafana" {
  alias         = "cloud"
  cloud_api_key = data.sops_file.secrets.data["grafana_cloud_org_key"]
}

resource "grafana_cloud_stack" "my_stack" {
  provider    = grafana.cloud
  name        = "sergiojvg92"
  slug        = "sergiojvg92"
  region_slug = "eu"
}


resource "grafana_cloud_stack_service_account" "cloud_sa" {
  provider   = grafana.cloud
  stack_slug = grafana_cloud_stack.my_stack.slug

  name        = "Terraform cloud service account"
  role        = "Admin"
  is_disabled = false
}

resource "grafana_cloud_stack_service_account_token" "cloud_sa" {
  provider   = grafana.cloud
  stack_slug = grafana_cloud_stack.my_stack.slug

  name               = "terraform cloud_sa key"
  service_account_id = grafana_cloud_stack_service_account.cloud_sa.id
}

provider "grafana" {
  alias = "my_stack"
  url   = grafana_cloud_stack.my_stack.url
  auth  = grafana_cloud_stack_service_account_token.cloud_sa.key
}

##### SYNTHETIC MONITORING
resource "grafana_cloud_access_policy" "sm_metrics_publish" {
  provider = grafana.cloud
  region   = "eu"
  name     = "metric-publisher-for-sm"

  scopes = ["metrics:write", "stacks:read"]

  realm {
    type       = "stack"
    identifier = grafana_cloud_stack.my_stack.id
  }
}

resource "grafana_cloud_access_policy_token" "sm_metrics_publish" {
  provider         = grafana.cloud
  region           = "eu"
  access_policy_id = grafana_cloud_access_policy.sm_metrics_publish.policy_id
  name             = "metric-publisher-for-sm"
}

resource "grafana_synthetic_monitoring_installation" "sm_stack" {
  provider              = grafana.cloud
  stack_id              = grafana_cloud_stack.my_stack.id
  metrics_publisher_key = grafana_cloud_access_policy_token.sm_metrics_publish.token
}

provider "grafana" {
  alias           = "sm"
  sm_access_token = grafana_synthetic_monitoring_installation.sm_stack.sm_access_token
  sm_url          = grafana_synthetic_monitoring_installation.sm_stack.stack_sm_api_url
}


data "terraform_remote_state" "infra" {
  backend = "http"

  config = {
    address = "https://gitlab.com/api/v4/projects/34632632/terraform/state/infra"
  }
}

data "sops_file" "secrets" {
  source_file = "secrets.json"
}
