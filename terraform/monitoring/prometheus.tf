#########################################################################
########################## PROMETHEUS SERVER ############################
#########################################################################

resource "helm_release" "prometheus" {
  name       = "prometheus"
  chart      = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  namespace  = "default"
  timeout    = "100"
  version    = var.versions.prometheus.chart

  values = [<<EOF
alertmanager:
  enabled: true
  ingress:
    enabled: true
    annotations: 
      kubernetes.io/ingress.class: nginx
    hosts:
      - host: alertmanager.my.house
        paths:
          - path: /
            pathType: ImplementationSpecific
  configmapReload:
    enabled: true
  persistence:
    enabled: false
  templates:
    telegram.tmpl: |-
      Type: {{.CommonAnnotations.description}}
      Summary: {{.CommonAnnotations.summary}}
      Alertname: {{ .CommonLabels.alertname }}
      Instance: {{ .CommonLabels.instance }}
      Serverity: {{ .CommonLabels.severity}}
      Status:  {{ .Status }}
  config:
    global:
      resolve_timeout: 5m
      http_config:
        follow_redirects: true
      smtp_hello: localhost
      smtp_require_tls: true
    templates:
      - '/etc/alertmanager/telegram.tmpl'
    receivers:
      - name: 'telegram'
        telegram_configs:
        - api_url: https://api.telegram.org
          send_resolved: true
          bot_token: ${data.sops_file.secrets.data["telegram_bot_api_key"]}
          chat_id: 527068743
          parse_mode: HTML
          send_resolved: true
    route:
      receiver: 'telegram'
      group_wait: 10s
      group_interval: 5m
      repeat_interval: 3h
      routes:
      - receiver: 'telegram'
        matchers:
        - severity="warning"
  resources:
    limits:
      cpu: 50m
      memory: 60Mi
    requests:
      cpu: 30m
      memory: 50Mi

kube-state-metrics:
  enabled: false

prometheus-node-exporter:
  enabled: false
  resources:
    limits:
      cpu: 400m
      memory: 120Mi
    requests:
      cpu: 100m
      memory: 110Mi

server:
  tsdb:
    out_of_order_time_window: 600s
  extraConfigmapMounts:
    - name: alerting-rules22
      mountPath: /etc/alert-rules
      subPath: ""
      configMap: ${kubernetes_config_map.alert_rules.metadata.0.name}
      readOnly: true
  resources:
    limits:
      cpu: 1000m
      memory: 1700Mi
    requests:
      cpu: 800m
      memory: 1700Mi
  ingress:
    enabled: true
    hosts:
      - prometheus.my.house

  persistentVolume:
    enabled: false

  defaultFlagsOverride:
    - --storage.tsdb.retention.time=2d
    - --config.file=/etc/config/prometheus.yml
    - --storage.tsdb.path=/data
    - --web.console.libraries=/etc/prometheus/console_libraries
    - --web.console.templates=/etc/prometheus/consoles
    - --web.enable-lifecycle
    - --web.enable-remote-write-receiver

  global:
    scrape_interval: 30s
    external_labels:
      cluster: ben

  #remoteWrite:
  #- url: ${data.grafana_cloud_stack.my_stack.prometheus_remote_write_endpoint}
  #  remote_timeout: 30s
  #  basic_auth:
  #    username: ${data.grafana_cloud_stack.my_stack.prometheus_user_id}
  #    password: ${grafana_cloud_api_key.prometheus.key}
  #  queue_config:
  #    capacity: 500
  #    max_shards: 1000
  #    min_shards: 1
  #    max_samples_per_send: 100
  #    batch_send_deadline: 5s
  #    min_backoff: 30ms
  #    max_backoff: 100ms
  #  write_relabel_configs:
  #    - source_labels: [__name__]
  #      #regex: kubelet_pod_worker_duration_seconds_count|container_fs_reads_bytes_total|container_network_receive_bytes_total|kubelet_node_config_error|rest_client_requests_total|node_namespace_pod_container:container_memory_cache|kubelet_certificate_manager_client_expiration_renew_errors|kubelet_volume_stats_inodes_used|kube_deployment_spec_replicas|kube_horizontalpodautoscaler_spec_min_replicas|kube_node_spec_taint|kubelet_running_containers|container_network_receive_packets_total|container_fs_writes_total|kubelet_pleg_relist_duration_seconds_count|namespace_workload_pod:kube_pod_owner:relabel|kube_namespace_status_phase|kube_resourcequota|kube_replicaset_owner|kubelet_running_container_count|storage_operation_duration_seconds_count|kube_node_status_capacity|kube_pod_info|kube_deployment_status_observed_generation|kube_pod_container_status_waiting_reason|kube_horizontalpodautoscaler_spec_max_replicas|kubelet_running_pod_count|kubelet_pleg_relist_interval_seconds_bucket|container_memory_rss|namespace_memory:kube_pod_container_resource_requests:sum|namespace_cpu:kube_pod_container_resource_requests:sum|namespace_cpu:kube_pod_container_resource_limits:sum|kube_node_status_condition|kubelet_node_name|volume_manager_total_volumes|kubernetes_build_info|kubelet_pod_start_duration_seconds_count|namespace_workload_pod|cluster:namespace:pod_memory:active:kube_pod_container_resource_limits|cluster:namespace:pod_cpu:active:kube_pod_container_resource_limits|kube_horizontalpodautoscaler_status_current_replicas|kubelet_cgroup_manager_duration_seconds_count|kube_daemonset_status_number_available|kube_statefulset_status_observed_generation|kube_statefulset_status_current_revision|kube_pod_container_resource_requests|node_namespace_pod_container:container_memory_swap|kube_node_info|kube_deployment_metadata_generation|kubelet_pod_worker_duration_seconds_bucket|kubelet_volume_stats_inodes|node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate|go_goroutines|cluster:namespace:pod_cpu:active:kube_pod_container_resource_requests|kube_statefulset_status_replicas_updated|container_memory_cache|kube_daemonset_status_updated_number_scheduled|process_resident_memory_bytes|kubelet_volume_stats_available_bytes|container_cpu_usage_seconds_total|kube_pod_status_phase|kubelet_server_expiration_renew_errors|storage_operation_errors_total|up|kubelet_volume_stats_capacity_bytes|container_memory_swap|node_namespace_pod_container:container_memory_working_set_bytes|kube_daemonset_status_desired_number_scheduled|kubelet_running_pods|kubelet_runtime_operations_errors_total|kube_pod_owner|kube_statefulset_replicas|kube_daemonset_status_current_number_scheduled|kube_statefulset_status_replicas|container_network_transmit_packets_total|container_network_transmit_packets_dropped_total|kubelet_runtime_operations_total|kube_node_status_allocatable|kube_statefulset_status_replicas_ready|kubelet_certificate_manager_client_ttl_seconds|kubelet_pleg_relist_duration_seconds_bucket|kube_deployment_status_replicas_available|kubelet_certificate_manager_server_ttl_seconds|container_network_transmit_bytes_total|node_namespace_pod_container:container_memory_rss|container_fs_reads_total|namespace_memory:kube_pod_container_resource_limits:sum|container_cpu_cfs_throttled_periods_total|kube_job_status_active|cluster:namespace:pod_memory:active:kube_pod_container_resource_requests|container_network_receive_packets_dropped_total|container_fs_writes_bytes_total|container_cpu_cfs_periods_total|kube_job_failed|kubelet_cgroup_manager_duration_seconds_bucket|kube_pod_container_resource_limits|kube_daemonset_status_number_misscheduled|kube_job_status_start_time|kube_horizontalpodautoscaler_status_desired_replicas|process_cpu_seconds_total|kube_deployment_status_replicas_updated|container_memory_working_set_bytes|node_quantile:kubelet_pleg_relist_duration_seconds:histogram_quantile|machine_memory_bytes|kube_statefulset_status_update_revision|kube_statefulset_metadata_generation|kube_namespace_status_phase|container_cpu_usage_seconds_total|kube_pod_status_phase|kube_pod_start_time|kube_pod_container_status_restarts_total|kube_pod_container_info|kube_pod_container_status_waiting_reason|kube_daemonset.*|kube_replicaset.*|kube_statefulset.*|kube_job.*
  #      regex: up|kubelet_node_config_error|kubernetes_build_info|container_network_receive_bytes_total|kubelet_volume_stats_available_bytes|kubelet_volume_stats_inodes_used|container_memory_cache|node_namespace_pod_container:container_memory_swap|kubelet_running_pod_count|container_memory_rss|node_namespace_pod_container:container_memory_rss|kubelet_volume_stats_capacity_bytes|kube_statefulset_status_observed_generation|rest_client_requests_total|kube_daemonset_status_current_number_scheduled|kube_statefulset_status_replicas_ready|namespace_memory:kube_pod_container_resource_limits:sum|container_memory_swap|kube_pod_info|container_cpu_cfs_periods_total|kubelet_pod_worker_duration_seconds_bucket|kube_job_failed|kubelet_running_containers|kube_pod_container_resource_limits|cluster:namespace:pod_memory:active:kube_pod_container_resource_limits|kubelet_node_name|kube_job_status_start_time|namespace_workload_pod:kube_pod_owner:relabel|kubelet_pod_start_duration_seconds_count|kubelet_runtime_operations_errors_total|kubelet_pleg_relist_duration_seconds_count|kubelet_runtime_operations_total|kubelet_pod_worker_duration_seconds_count|kubelet_server_expiration_renew_errors|kubelet_volume_stats_inodes|kube_horizontalpodautoscaler_status_desired_replicas|namespace_memory:kube_pod_container_resource_requests:sum|kube_deployment_status_replicas_updated|kube_statefulset_replicas|container_network_transmit_bytes_total|cluster:namespace:pod_cpu:active:kube_pod_container_resource_limits|container_fs_writes_total|kube_node_status_allocatable|kube_horizontalpodautoscaler_status_current_replicas|container_cpu_usage_seconds_total|kubelet_pleg_relist_duration_seconds_bucket|kube_node_status_condition|process_resident_memory_bytes|kube_daemonset_status_number_misscheduled|kube_daemonset_status_number_available|namespace_cpu:kube_pod_container_resource_requests:sum|container_fs_reads_bytes_total|namespace_workload_pod|kube_pod_container_resource_requests|kubelet_running_pods|container_network_receive_packets_total|kube_statefulset_status_update_revision|kube_deployment_status_replicas_available|kube_node_spec_taint|node_namespace_pod_container:container_memory_working_set_bytes|kubelet_pod_start_duration_seconds_bucket|kube_deployment_metadata_generation|kube_statefulset_metadata_generation|kube_pod_container_status_waiting_reason|node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate|container_network_transmit_packets_total|container_cpu_cfs_throttled_periods_total|node_namespace_pod_container:container_memory_cache|kube_pod_status_phase|kubelet_certificate_manager_server_ttl_seconds|container_network_receive_packets_dropped_total|kube_node_status_capacity|process_cpu_seconds_total|kubelet_cgroup_manager_duration_seconds_bucket|kube_daemonset_status_desired_number_scheduled|kube_pod_owner|kube_node_info|kube_statefulset_status_replicas|node_quantile:kubelet_pleg_relist_duration_seconds:histogram_quantile|cluster:namespace:pod_cpu:active:kube_pod_container_resource_requests|kube_resourcequota|kubelet_running_container_count|kube_namespace_status_phase|storage_operation_duration_seconds_count|kubelet_cgroup_manager_duration_seconds_count|machine_memory_bytes|volume_manager_total_volumes|kube_daemonset_status_updated_number_scheduled|namespace_cpu:kube_pod_container_resource_limits:sum|cluster:namespace:pod_memory:active:kube_pod_container_resource_requests|kube_statefulset_status_replicas_updated|container_network_transmit_packets_dropped_total|container_fs_reads_total|kube_job_status_active|kube_statefulset_status_current_revision|container_memory_working_set_bytes|kubelet_pleg_relist_interval_seconds_bucket|kube_horizontalpodautoscaler_spec_min_replicas|kube_deployment_spec_replicas|container_fs_writes_bytes_total|kube_deployment_status_observed_generation|kube_horizontalpodautoscaler_spec_max_replicas|kubelet_certificate_manager_client_ttl_seconds|go_goroutines|storage_operation_errors_total|kubelet_certificate_manager_client_expiration_renew_errors|kube_replicaset_owner|kube_namespace_status_phase|container_cpu_usage_seconds_total|kube_pod_status_phase|kube_pod_start_time|kube_pod_container_status_restarts_total|kube_pod_container_info|kube_pod_container_status_waiting_reason|kube_daemonset.*|kube_replicaset.*|kube_statefulset.*|kube_job.*|kube_node.*|node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate|cluster:namespace:pod_cpu:active:kube_pod_container_resource_requests|namespace_cpu:kube_pod_container_resource_requests:sum
  #      action: keep

### We do not need pushgateway
pushgateway:
  enabled: false

serverFiles:
  alerting_rules.yml: {}
  prometheus.yml:
    rule_files:
      - /etc/config/recording_rules.yml
      - /etc/config/alerting_rules.yml
      - /etc/alert-rules/alerting-rules-extended.yml
      - /etc/config/rules
      - /etc/config/alerts

    scrape_configs:
      - job_name: prometheus
        static_configs:
          - targets:
            - localhost:9090
EOF
  ]
}

data "kubernetes_service" "prometheus-server" {
  metadata {
    name = "prometheus-server"
  }
  depends_on = [helm_release.prometheus]
}
