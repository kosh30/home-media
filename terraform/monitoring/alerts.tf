resource "kubernetes_config_map" "alert_rules" {
  metadata {
    name = "alert-rules"
  }

  data = {
    "alerting-rules-extended.yml" = <<EOF
groups:
  #- name: MetalLB
  #  rules:
  #   - alert: MetalLBStaleConfig
  #     annotations:
  #       description:  {{ $labels.job }} - MetalLB {{ $labels.container }} on {{ $labels.pod }} has a stale config for > 1 minute
  #     expr:       metallb_k8s_client_config_stale_bool{job="metallb"} == 1
  #     for:        1m
  #     labels:
  #       severity:  warning
  #   - alert: MetalLBConfigNotLoaded
  #     annotations:
  #       description:  {{ $labels.job }} - MetalLB {{ $labels.container }} on {{ $labels.pod }} has not loaded for > 1 minute
  #     expr:       metallb_k8s_client_config_loaded_bool{job="metallb"} == 0
  #     for:        1m
  #     labels:
  #       severity:  warning
  #   - alert: MetalLBAddressPoolExhausted
  #     annotations:
  #       description:  {{ $labels.job }} - MetalLB {{ $labels.container }} on {{ $labels.pod }} has exhausted address pool {{ $labels.pool }} for > 1 minute
  #     expr:       metallb_allocator_addresses_in_use_total >= on(pool) metallb_allocator_addresses_total
  #     for:        1m
  #     labels:
  #       severity:  alert
  #   - alert: MetalLBAddressPoolUsage75Percent
  #     annotations:
  #       description:  {{ $labels.job }} - MetalLB {{ $labels.container }} on {{ $labels.pod }} has address pool {{ $labels.pool }} past 75% usage for > 1 minute
  #     expr:       ( metallb_allocator_addresses_in_use_total / on(pool) metallb_allocator_addresses_total ) * 100 > 75
  #     for:        1m
  #     labels:
  #       severity:  warning
  - name: Homeassistant
    rules:
      - alert: Device_low_on_battery
        expr: homeassistant_sensor_battery_percent{entity!~"sensor.kentia_palm_1_battery|sensor.livistona_rotundifolia_1_battery|sensor.polyscias_guifoylei_1_battery"} < 4
        for: 0m
        labels:
          severity: warning
        annotations:
          summary: 'Smart device {{ $labels.friendly_name }} has {{ $value }}% of battery'
          description: 'Smart device {{ $labels.friendly_name }} ({{ $labels.entity}}) has {{ $value }}% of battery'
  - name: Blackbox exporter
    rules:
      - alert: Blackbox exporter service down
        expr: probe_success == 0
        for: 0m
        labels:
          severity: warning
        annotations:
          summary: 'Blackbox exporter service down (instance {{ $labels.instance }}, job {{ $labels.job }})'
          description: 'Service down (instance {{ $labels.instance }}, job {{ $labels.job }})'
  #- name: Prometheus Self
  #  rules:
  #    - alert: PrometheusJobMissing
  #      expr: absent(up{job="prometheus"})
  #      for: 0m
  #      labels:
  #        severity: warning
  #      annotations:
  #        summary: 'Prometheus job missing (instance {{ $labels.instance }})'
  #        description: 'A Prometheus job has disappeared\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
  #    - alert: PrometheusTargetMissing
  #      expr: up == 0
  #      for: 0m
  #      labels:
  #        severity: critical
  #      annotations:
  #        summary: 'Prometheus target missing (instance {{ $labels.instance }})'
  #        description: 'A Prometheus target has disappeared. An exporter might be crashed.\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
  - name: kube-state-metrics
    rules:
      #- alert: KubernetesNodeReady
      #  expr: kube_node_status_condition{condition="Ready",status="true"} == 0
      #  for: 10m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes Node ready (instance {{ $labels.instance }})
      #    description: "Node {{ $labels.node }} has been unready for a long time\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesMemoryPressure
      #  expr: kube_node_status_condition{condition="MemoryPressure",status="true"} == 1
      #  for: 2m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes memory pressure (instance {{ $labels.instance }})
      #    description: "{{ $labels.node }} has MemoryPressure condition\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesDiskPressure
      #  expr: kube_node_status_condition{condition="DiskPressure",status="true"} == 1
      #  for: 2m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes disk pressure (instance {{ $labels.instance }})
      #    description: "{{ $labels.node }} has DiskPressure condition\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesOutOfDisk
      #  expr: kube_node_status_condition{condition="OutOfDisk",status="true"} == 1
      #  for: 2m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes out of disk (instance {{ $labels.instance }})
      #    description: "{{ $labels.node }} has OutOfDisk condition\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesOutOfCapacity
      #  expr: sum by (node) ((kube_pod_status_phase{phase="Running"} == 1) + on(uid) group_left(node) (0 * kube_pod_info{pod_template_hash=""})) / sum by (node) (kube_node_status_allocatable{resource="pods"}) * 100 > 90
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes out of capacity (instance {{ $labels.instance }})
      #    description: "{{ $labels.node }} is out of capacity\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesContainerOomKiller
      #  expr: (kube_pod_container_status_restarts_total - kube_pod_container_status_restarts_total offset 10m >= 1) and ignoring (reason) min_over_time(kube_pod_container_status_last_terminated_reason{reason="OOMKilled"}[10m]) == 1
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes container oom killer (instance {{ $labels.instance }})
      #    description: "Container {{ $labels.container }} in pod {{ $labels.namespace }}/{{ $labels.pod }} has been OOMKilled {{ $value }} times in the last 10 minutes.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesJobFailed
      #  expr: kube_job_status_failed > 0
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes Job failed (instance {{ $labels.instance }})
      #    description: "Job {{$labels.namespace}}/{{$labels.exported_job}} failed to complete\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesCronjobSuspended
      #  expr: kube_cronjob_spec_suspend != 0
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes CronJob suspended (instance {{ $labels.instance }})
      #    description: "CronJob {{ $labels.namespace }}/{{ $labels.cronjob }} is suspended\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesPersistentvolumeclaimPending
      #  expr: kube_persistentvolumeclaim_status_phase{phase="Pending"} == 1
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes PersistentVolumeClaim pending (instance {{ $labels.instance }})
      #    description: "PersistentVolumeClaim {{ $labels.namespace }}/{{ $labels.persistentvolumeclaim }} is pending\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesVolumeOutOfDiskSpace
      #  expr: kubelet_volume_stats_available_bytes / kubelet_volume_stats_capacity_bytes * 100 < 10
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes Volume out of disk space (instance {{ $labels.instance }})
      #    description: "Volume is almost full (< 10% left)\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      - alert: KubernetesVolumeOutOfDiskSpace - Otel
        expr: sum by(k8s_volume_name,k8s_pod_name,k8s_namespace_name) (k8s_volume_available_bytes{k8s_volume_type=~"persistentVolumeClaim|emptyDir"}) / sum by(k8s_volume_name,k8s_pod_name,k8s_namespace_name) (k8s_volume_capacity_bytes{k8s_volume_type=~"persistentVolumeClaim|emptyDir"}) < 0.12
        for: 1m
        labels:
          severity: warning
        annotations:
          summary: '{{ $labels.k8s_pod_name }}-{{ $labels.k8s_volume_name}} has less than 12% of disk space left'
          description: "Volume {{ $labels.k8s_volume_name}} from pod {{ $labels.k8s_pod_name}} is almost full (< 12% left)\n  VALUE = {{ $value }}\n"
      #- alert: KubernetesPersistentvolumeError
      #  expr: kube_persistentvolume_status_phase{phase=~"Failed|Pending", job="kube-state-metrics"} > 0
      #  for: 0m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes PersistentVolume error (instance {{ $labels.instance }})
      #    description: "Persistent volume is in bad state\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesPodNotHealthy
      #  expr: min_over_time(sum by (namespace, pod) (kube_pod_status_phase{phase=~"Pending|Unknown|Failed", pod!~"svclb.*", pod!~"helm-install-traefik-.*"})[15m:1m]) > 0
      #  for: 0m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes Pod not healthy (instance {{ $labels.instance }})
      #    description: "Pod has been in a non-ready state for longer than 15 minutes.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesPodRestarted
      #  expr: delta(kube_pod_container_status_restarts_total{pod!~"helm-install-traefik-.*"}[2m]) > 1
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: A Pod just restarted (instance {{ $labels.instance }})
      #    description: "Pod {{ $labels.pod }} restarted\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      - alert: KubernetesContainerRestarted - Otel  
        expr: delta(k8s_container_restarts[1m]) > 1
        for: 0m
        labels:
          severity: warning
        annotations:
          summary: 'Container restarted POD:{{ $labels.k8s_pod_name }}. Container: {{ $labels.k8s_container_name }}. 1min restart rate = {{ $value }}'
      - alert: KubernetesContainerNotReady - Otel  
        expr: k8s_container_ready != 1 
        for: 5m
        labels:
          severity: warning
        annotations:
          summary: 'Container {{ $labels.k8s_container_name }} from POD:{{ $labels.k8s_pod_name }} using image {{ $labels.container_image_name }} has not been started for 5 minutes'
      #- alert: KubernetesPodCrashLooping
      #  expr: increase(kube_pod_container_status_restarts_total[1m]) > 2
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes pod crash looping (instance {{ $labels.instance }})
      #    description: "Pod {{ $labels.pod }} is crash looping\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesDeploymentReplicasMismatch
      #  expr: kube_deployment_spec_replicas != kube_deployment_status_replicas_available
      #  for: 10m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes Deployment replicas mismatch (instance {{ $labels.instance }})
      #    description: "Deployment Replicas mismatch\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesDeploymentGenerationMismatch
      #  expr: kube_deployment_status_observed_generation != kube_deployment_metadata_generation
      #  for: 10m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes Deployment generation mismatch (instance {{ $labels.instance }})
      #    description: "A Deployment has failed but has not been rolled back.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesCronjobTooLong
      #  expr: time() - kube_cronjob_next_schedule_time > 3600
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Kubernetes CronJob too long (instance {{ $labels.instance }})
      #    description: "CronJob {{ $labels.namespace }}/{{ $labels.cronjob }} is taking more than 1h to complete.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: KubernetesApiServerErrors
      #  expr: sum(rate(apiserver_request_total{job="apiserver",code=~"^(?:5..)$"}[1m])) / sum(rate(apiserver_request_total{job="apiserver"}[1m])) * 100 > 3
      #  for: 2m
      #  labels:
      #    severity: critical
      #  annotations:
      #    summary: Kubernetes API server errors (instance {{ $labels.instance }})
      #    description: "Kubernetes API server is experiencing high error rate\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
  - name: Infrastructure
    rules:
      #- alert: HostOutOfMemory
      #  expr: node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes * 100 < 12
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Host out of memory (instance {{ $labels.instance }})'
      #    description: 'Node memory is filling up (< 12% left) for 2 minutes \n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      #- alert: HostOutOfFreeMemory - Otel
      #  expr: system_memory_utilization{state="free"} < 0.08
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Host out of free memory (instance {{ $labels.k8s_node_name }})'
      #    description: 'Node memory is filling up (< 8% left) for 2 minutes \n  VALUE = {{ $value }}\n'
      - alert: HostUsedMemory - Otel
        expr: system_memory_utilization_ratio{state="used"} > 0.85
        for: 2m
        labels:
          severity: warning
        annotations:
          summary: 'Host {{ $labels.k8s_node_name }} used more than 80% of memory'
          description: 'Node memory is filling up. At least {{ $value }}% used for 2 minutes \n'
      #
      #- alert: HostOutOfDiskSpace
      #  expr: (node_filesystem_avail_bytes{mountpoint !~ "/var/log.*"} * 100) / node_filesystem_size_bytes{mountpoint !~ "/var/log.*"} < 10 and ON (instance, device, mountpoint) node_filesystem_readonly == 0
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Host out of disk space (instance {{ $labels.instance }})
      #    description: "Disk is almost full (< 10% left)\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      - alert: HostOutOfDiskSpace - Otel
        expr: system_filesystem_utilization_ratio{mode="rw"} > 0.9
        #expr: (k8s_node_filesystem_available_bytes{mountpoint !~ "/var/log.*"} * 100) / node_filesystem_size_bytes{mountpoint !~ "/var/log.*"} < 10 and ON (instance, device, mountpoint) node_filesystem_readonly == 0
        for: 1m
        labels:
          severity: warning
        annotations:
          summary: Host out of disk space (<90%) (instance {{ $labels.k8s_node_name }})
          description: "Disk is almost full (< 10% left)\n on node {{ $labels.k8s_node_name }} VALUE = {{ $value }}\n"
      #- alert: HostHighCpuLoad
      #  expr: 100 - (avg by(instance) (rate(node_cpu_seconds_total{mode="idle"}[2m])) * 100) > 80
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Host high CPU load (instance {{ $labels.instance }})'
      #    description: 'CPU load is > 80% for 2 minutes \n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      - alert: HostHighCpuLoad - Otel
        expr: avg by (state,k8s_node_name) (system_cpu_utilization_ratio{state="idle"}) < 0.3
        for: 2m
        labels:
          severity: warning
        annotations:
          summary: 'Host high CPU usage (instance {{ $labels.k8s_node_name }})'
          description: 'CPU was less than 30% on idle state for 2 minutes on {{ $labels.k8s_node_name }} \n  VALUE = {{ $value }}\n'  
      - alert: KubernetesNodeMemoryPressure - Otel
        expr: k8s_node_condition_memory_pressure != 0
        for: 0m
        labels:
          severity: warning
        annotations:
          summary: 'Node {{ $labels.k8s_node_name }} is reporting memory pressure'
      - alert: KubernetesNodeNotReady - Otel
        expr: k8s_node_condition_ready != 1
        for: 0m
        labels:
          severity: warning
        annotations:
          summary: 'Node {{ $labels.k8s_node_name }} is not in READY state'
      #- alert: HostPhysicalComponentTooHot
      #  expr: node_hwmon_temp_celsius > 60
      #  for: 5m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Host physical component too hot (instance {{ $labels.instance }})'
      #    description: 'Physical hardware component too hot (60 degrees) \n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      #- alert: HostOomKillDetected
      #  expr: increase(node_vmstat_oom_kill[2m]) > 0
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Host OOM kill detected (instance {{ $labels.instance }})'
      #    description: 'OOM kill detected\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      #- alert: HostClockNotSynchronising
      #  expr: min_over_time(node_timex_sync_status[2m]) == 0 and node_timex_maxerror_seconds >= 16
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Host clock not synchronising (instance {{ $labels.instance }})'
      #    description: 'Clock not synchronising.\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      #- alert: HostOomKillDetected
      #  expr: increase(node_vmstat_oom_kill[1m]) > 0
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Host OOM kill detected (instance {{ $labels.instance }})
      #    description: "OOM kill detected\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: HostClockSkew
      #  expr: (node_timex_offset_seconds > 0.05 and deriv(node_timex_offset_seconds[5m]) >= 0) or (node_timex_offset_seconds < -0.05 and deriv(node_timex_offset_seconds[5m]) <= 0)
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Host clock skew (instance {{ $labels.instance }})
      #    description: "Clock skew detected. Clock is out of sync. Ensure NTP is configured correctly on this host.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
      #- alert: HostClockNotSynchronising
      #  expr: min_over_time(node_timex_sync_status[1m]) == 0 and node_timex_maxerror_seconds >= 16
      #  for: 2m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: Host clock not synchronising (instance {{ $labels.instance }})
      #    description: "Clock not synchronising. Ensure NTP is configured on this host.\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
  - name: cAdvisor
    rules:
      ##- alert: ContainerCpuUsage_requests
      ##  #expr: (rate(container_cpu_usage_seconds_total{image!="",container!="",container!="prometheus-server",io_kubernetes_container_name!="POD"}[2m])/ ON(pod,container) kube_pod_container_resource_requests{unit="core"}) > 0.95
      ##  expr: (rate(container_cpu_usage_seconds_total{image!="",container!="",namespace!="longhorn-system"}[2m])/ ON(pod,container,namespace) kube_pod_container_resource_requests{unit="core"}) > 0.95
      ##  for: 1m
      ##  labels:
      ##    severity: warning
      ##  annotations:
      ##    summary: 'Container CPU usage is over 95% of request during 1 minute (instance {{ $labels.instance }})'
      ##    description: 'Container CPU usage is above 80% of request\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      #- alert: ContainerCpuUsage_limits
      #  #expr: (rate(container_cpu_usage_seconds_total{image!="",container!="",container!="prometheus-server",io_kubernetes_container_name!="POD"}[2m])/ ON(pod,container) kube_pod_container_resource_limits{unit="core"}) > 0.80
      #  expr: (rate(container_cpu_usage_seconds_total{image!="",container!="",namespace!="longhorn-system"}[2m])/ ON(pod,container,namespace) kube_pod_container_resource_limits{unit="core"}) > 0.80
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Container CPU usage is over 80% of limits (instance {{ $labels.instance }})'
      #    description: 'Container CPU usage is above 80% of limits\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      #- alert: ContainerCpuUsage_limits
      - alert: ContainerCpuUsage_limits - Otel
        expr: container_cpu_utilization_ratio > 0.92
        for: 10m
        labels:
          severity: warning
        annotations:
          summare: 'High CPU usage {{ $labels.k8s_pod_name }}-{{ $labels.k8s_container_name}} > 92%' 
          description: 'CPU usage for container {{ $labels.k8s_container_name}} in pod {{ $labels.k8s_pod_name }} had an average of {{ $value }} for 10 minutes against its limit'
      #- alert: ContainerMemoryUsage_requests
      #  expr: (sum by(container) (container_memory_working_set_bytes{container!="",id!="/"}) * 100 / sum by(container,namespace) (kube_pod_container_resource_requests{container!="",id!="/"})) > 95
      #  for: 1m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Container Memory usage requests is over 95% during 1 minute(instance {{ $labels.instance }} - container {{ $labels.container }})'
      #    description: 'Container Memory usage requests is above 90%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      #- alert: ContainerMemoryUsage_limits
      #  expr: (sum by(container) (container_memory_working_set_bytes{container!="",id!="/"}) * 100 / sum by(container,namespace) (kube_pod_container_resource_limits{container!="",id!="/"})) > 85
      #  for: 0m
      #  labels:
      #    severity: warning
      #  annotations:
      #    summary: 'Container Memory usage limits (instance {{ $labels.instance }} - container {{ $labels.container }})'
      #    description: 'Container Memory usage limits is above 80%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
      - alert: ContainerMemoryUsage_limits - Otel
        expr: sum by(k8s_container_name,k8s_pod_name,k8s_namespace_name) (container_memory_usage_bytes) / sum by(k8s_container_name,k8s_pod_name,k8s_namespace_name) (k8s_container_memory_limit_bytes) > 0.92
        for: 10m
        labels:
          severity: warning
        annotations:
          summary: 'High Memory usage {{ $labels.k8s_pod_name }}-{{ $labels.k8s_container_name}} > 92%'
          description: 'Memory usage for container {{ $labels.k8s_container_name}} in pod {{ $labels.k8s_pod_name }} had an average of {{ $value }} for 10 minutes against its limit'
      #- alert: ContainerMemoryUsage_requests - Otel
      #  expr: sum by(k8s_container_name,k8s_pod_name,k8s_namespace_name) (container_memory_usage_bytes) / sum by(k8s_container_name,k8s_pod_name,k8s_namespace_name) (k8s_container_memory_request_bytes) > 0.85
      #  for: 5m
      #  labels:
      #    severity: info
      #  annotations:
      #    summary: 'High Memory usage (85%) {{ $labels.k8s_pod_name }}-{{ $labels.k8s_container_name}}'
      #    description: 'Memory usage for container {{ $labels.k8s_container_name}} in pod {{ $labels.k8s_pod_name }} has been over 85% against its request for 1 minute'
      ##- alert: ContainerVolumeIoUsage
  - name: node-problem-detector
    rules:
      - alert: TaskHung
        expr: increase(problem_counter_total{reason="TaskHung"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Task hung'
          description: 'Task hung in node {{ $labels.node }}'
      - alert: NodeProblemOOMKilling
        expr: increase(problem_counter_total{reason="OOMKilling"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Process Out-Of-Memory Killed'
          description: 'Process Out-Of-Memory Killed in node {{ $labels.node }}'
      - alert: NodeProblemUnregisterNetDevice
        expr: increase(problem_counter_total{reason="UnregisterNetDevice"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Unregister Network Device'
          description: 'Unable to unregister a network device in node {{ $labels.node }}'
      - alert: NodeProblemMemoryReadError
        expr: increase(problem_counter_total{reason="MemoryReadError"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Memory Read Error'
          description: 'CE memory read error in node {{ $labels.node }}'
      - alert: NodeProblemKernelOops
        expr: increase(problem_counter_total{reason="KernelOops"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Kernel oops'
          description: 'Kernel oops detected in node {{ $labels.node }}'
      - alert: NodeProblemIOError
        expr: increase(problem_counter_total{reason="IOError"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Buffer I/O'
          description: 'Buffer I/O error in node {{ $labels.node }}'
      - alert: NodeProblemFilesystemIsReadOnly
        expr: increase(problem_counter_total{reason="FilesystemIsReadOnly"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Filesystem is read only'
          description: 'Filesystem is read only in node {{ $labels.node }}'
      - alert: NodeProblemExt4Warning
        expr: increase(problem_counter_total{reason="Ext4Warning"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'EXT4-fs warning'
          description: 'EXT4-fs warning in node {{ $labels.node }}'
      - alert: NodeProblemExt4Error
        expr: increase(problem_counter_total{reason="Ext4Error"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'EXT4-fs error'
          description: 'EXT4-fs error in node {{ $labels.node }}'
      - alert: NodeProblemDockerHung
        expr: increase(problem_counter_total{reason="DockerHung"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Docker hung'
          description: 'Docker hung in node {{ $labels.node }}'
      - alert: NodeProblemDockerContainerStartupFailure
        expr: increase(problem_counter_total{reason="DockerContainerStartupFailure"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Docker container startup failure'
          description: 'Docker container startup failure in node {{ $labels.node }}'
      - alert: NodeProblemCorruptDockerOverlay2
        expr: increase(problem_counter_total{reason="CorruptDockerOverlay2"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Corrupt docker overlay2'
          description: 'Corrupt docker overlay2 in node {{ $labels.node }}'
      - alert: NodeProblemCorruptDockerImage
        expr: increase(problem_counter_total{reason="CorruptDockerImage"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Corrupt docker image'
          description: 'Corrupt docker image in node {{ $labels.node }}'
      - alert: NodeProblemGaugeCorruptDockerOverlay2
        expr: increase(problem_gauge{reason="CorruptDockerOverlay2",type="CorruptDockerOverlay2"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Corrupt docker overlay2'
          description: 'Corrupt docker overlay2 is affecting node {{ $labels.node }}'
      - alert: NodeProblemGaugeDockerHung
        expr: increase(problem_gauge{reason="DockerHung",type="KernelDeadlock"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Docker hung'
          description: 'Docker hung (Kernel deadlock) is affecting node {{ $labels.node }}'
      - alert: NodeProblemGaugeFilesystemIsReadOnly
        expr: increase(problem_gauge{reason="FilesystemIsReadOnly",type="ReadonlyFilesystem"}[2m]) > 0
        for: 1s
        labels:
          severity: warning
        annotations:
          summary: 'Filesystem is read only'
          description: 'Read only filesystem is affecting node {{ $labels.node }}'
EOF
  }
}
