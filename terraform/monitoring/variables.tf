variable "versions" {
  type = object({
    grafana = object({
      chart = string
    })
    prometheus = object({
      chart = string
    })
    prometheus_blackbox_exporter = object({
      chart = string
    })
    promtail = object({
      chart = string
    })
    otel_collector = object({
      chart = string
    })
    node_problem_detector = object({
      chart = string
    })
  })
}
