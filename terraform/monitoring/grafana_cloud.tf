resource "grafana_cloud_api_key" "loki" {
  provider = grafana.cloud

  cloud_org_slug = grafana_cloud_stack.my_stack.slug
  name           = "loki"
  role           = "MetricsPublisher"
}

resource "grafana_cloud_api_key" "prometheus" {
  provider = grafana.cloud

  cloud_org_slug = grafana_cloud_stack.my_stack.slug
  name           = "prometheus"
  role           = "MetricsPublisher"
}

data "grafana_cloud_stack" "my_stack" {
  provider = grafana.cloud
  slug     = grafana_cloud_stack.my_stack.slug
}
#
#resource "grafana_data_source" "prometheus" {
#  provider            = grafana.my_stack
#  type                = "prometheus"
#  name                = "prometheus-my-house"
#  url                 = data.grafana_cloud_stack.my_stack.prometheus_remote_write_endpoint
#  basic_auth_enabled  = true
#  basic_auth_username = data.grafana_cloud_stack.my_stack.prometheus_user_id
#  secure_json_data_encoded = jsonencode({
#    basicAuthPassword = grafana_cloud_api_key.prometheus.key
#  })
#  is_default = false
#
#  json_data_encoded = jsonencode({
#    http_method     = "POST"
#    sigv4_auth      = true
#    sigv4_auth_type = "default"
#    sigv4_region    = "eu-west-1"
#  })
#}
#
#resource "grafana_data_source" "loki" {
#  provider            = grafana.my_stack
#  type                = "loki"
#  name                = "loki-my-home"
#  url                 = "${data.grafana_cloud_stack.my_stack.logs_url}/api/prom/push"
#  basic_auth_enabled  = true
#  basic_auth_username = data.grafana_cloud_stack.my_stack.logs_user_id
#  secure_json_data_encoded = jsonencode({
#    basicAuthPassword = grafana_cloud_api_key.loki.key
#  })
#  is_default = false
#}
#
resource "grafana_folder" "home" {
  provider = grafana.my_stack
  title    = "My_Home"
}
resource "grafana_dashboard" "home" {
  provider    = grafana.my_stack
  config_json = file("../../dashboards/grafana/home.json")
  folder      = grafana_folder.home.id
}

#### ALERTS
resource "grafana_contact_point" "telegram" {
  provider = grafana.my_stack
  name     = "telegram"

  telegram {
    chat_id = "527068743"
    token   = data.sops_file.secrets.data["telegram_bot_api_key"]
  }
}

resource "grafana_notification_policy" "warning" {
  provider      = grafana.my_stack
  group_by      = ["..."]
  contact_point = grafana_contact_point.telegram.name

  group_wait      = "45s"
  group_interval  = "6m"
  repeat_interval = "3h"
}

resource "grafana_folder" "custom_rules" {
  provider = grafana.my_stack
  title    = "Alerts"
}

resource "grafana_rule_group" "services_up" {
  provider         = grafana.my_stack
  name             = "Service State"
  folder_uid       = grafana_folder.custom_rules.uid
  interval_seconds = 60
  rule {
    name           = "Service Down"
    for            = "5m"
    condition      = "C"
    no_data_state  = "NoData"
    exec_err_state = "Alerting"
    labels = {
      "probe"    = "{{ $values.C.Labels.probe }}"
      "job"      = "{{ $values.C.Labels.job }}"
      "instance" = "{{ $values.C.Labels.instance }}"
    }
    data {
      ref_id = "A"
      relative_time_range {
        from = 600
        to   = 0
      }
      datasource_uid = "grafanacloud-prom"
      model          = <<EOT
        {
          "expr": "probe_success",
					"legendFormat": "{{ instance }} :: {{ job }} - {{ probe }}",
          "intervalMs": 1000,
          "maxDataPoints": 43200,
          "refId": "A"
        }
EOT
    }
    data {
      ref_id     = "B"
      query_type = ""
      relative_time_range {
        from = 0
        to   = 0
      }
      datasource_uid = "-100"
      model          = <<EOT
{
            "datasource":{"name":"Expression","type":"__expr__","uid":"__expr__"},
						"hide": false,
						"expression": "A",
						"intervalMs": 1000,
						"maxDataPoints": 43200,
						"reducer": "last",
						"refId": "B",
						"type": "reduce"
}
EOT
    }
    data {
      datasource_uid = "-100"
      ref_id         = "C"
      relative_time_range {
        from = 0
        to   = 0
      }
      model = jsonencode({
        expression    = "$B == 0"
        type          = "math"
        intervalMs    = 1000
        maxDataPoints = 43200
        refId         = "C"
      })
    }
  }
}

data "grafana_synthetic_monitoring_probes" "main" {
  provider = grafana.sm
  depends_on = [
    grafana_synthetic_monitoring_installation.sm_stack
  ]
}

resource "grafana_synthetic_monitoring_check" "homeassistant" {
  provider  = grafana.sm
  job       = "homeassistant automated"
  target    = "https://homeassistant.${data.terraform_remote_state.infra.outputs.public_domain}"
  enabled   = true
  frequency = 60000
  probes = [
    data.grafana_synthetic_monitoring_probes.main.probes.Amsterdam,
  ]
  labels = {
    app = "homeassistant"
  }
  settings {
    http {}
  }
}

resource "grafana_synthetic_monitoring_check" "qnap" {
  provider  = grafana.sm
  job       = "qnap automated"
  target    = "https://qnap.${data.terraform_remote_state.infra.outputs.public_domain}"
  enabled   = true
  frequency = 60000
  timeout   = 6000
  probes = [
    data.grafana_synthetic_monitoring_probes.main.probes.Amsterdam,
  ]
  labels = {
    app = "qnap"
  }
  settings {
    http {}
  }
}

resource "grafana_synthetic_monitoring_check" "tcp" {
  provider  = grafana.sm
  job       = "TCP Defaults"
  target    = "${data.terraform_remote_state.infra.outputs.public_ip}:443"
  enabled   = true
  frequency = 60000
  timeout   = 6000
  probes = [
    data.grafana_synthetic_monitoring_probes.main.probes.Amsterdam,
    data.grafana_synthetic_monitoring_probes.main.probes.London,
  ]
  labels = {
    app = "port443"
  }
  settings {
    tcp {}
  }
}
