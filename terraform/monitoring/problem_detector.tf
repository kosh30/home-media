resource "helm_release" "node_problem_detector" {
  name       = "node-problem-detector"
  chart      = "node-problem-detector"
  repository = "https://charts.deliveryhero.io"
  namespace  = "default"
  version    = var.versions.node_problem_detector.chart
  timeout    = "300"

  values = [<<EOF
image:
  #repository: registry.k8s.io/node-problem-detector/node-problem-detector
  tag: v0.8.13 ### 0.8.14 is broken 
resources:
  requests:
    cpu: 100m
    memory: 50Mi
  limits:
    cpu: 100m
    memory: 100Mi
metrics:
  enabled: true
  serviceMonitor:
    enabled: true
    additionalLabels: {}
    additionalRelabelings: []
    metricRelabelings: []
  prometheusRule:
    enabled: true
    defaultRules:
      create: true
      disabled: []
    additionalLabels: {}
    additionalRules: []
EOF
  ]
}
