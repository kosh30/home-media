#########################################################################
############################### GRAFANA #################################
#########################################################################


resource "helm_release" "grafana" {
  name       = "grafana"
  chart      = "grafana"
  repository = "https://grafana.github.io/helm-charts"
  namespace  = "default"
  timeout    = "300"
  version    = var.versions.grafana.chart

  values = [<<EOF
ingress:
  enabled: true
  annotations: 
    kubernetes.io/ingress.class: nginx
  hosts:
    - grafana.${data.terraform_remote_state.infra.outputs.local_domain}

adminUser: kernel
adminPassword: ${data.sops_file.secrets.data["grafana_admin_password"]}

rbac:
  pspEnabled: false

resources:
  limits:
    cpu: 600m
    memory: 250Mi
  requests:
    cpu: 400m
    memory: 250Mi

readinessProbe:
  initialDelaySeconds: 180

livenessProbe:
  initialDelaySeconds: 180

plugins:
  - grafana-piechart-panel

grafana.ini:
  paths:
    data: /var/lib/grafana/data
    logs: /var/log/grafana
    plugins: /var/lib/grafana/plugins
    provisioning: /etc/grafana/provisioning
  analytics:
    check_for_updates: true
  log:
    mode: console
  security:
    allow_embedding: true
  auth:
    disable_login_form: false
  auth.anonymous:
    enabled: false
    #org_name: Main Org.
    #org_role: Viewer
    hide_version: true
  grafana_net:
    url: https://grafana.net

datasources:
  datasources.yaml:
    apiVersion: 1
    datasources:
    - name: Prometheus
      type: prometheus
      url: http://hardcoded
      access: proxy
      isDefault: true

dashboardProviders:
  dashboardproviders.yaml:
    apiVersion: 1
    providers:
    - name: 'default'
      orgId: 1
      folder: ''
      type: file
      disableDeletion: false
      editable: true
      options:
        path: /var/lib/grafana/dashboards/default

dashboards:
  default:
    home:
      url: https://gitlab.com/sergiojvg/home-media/-/raw/main/dashboards/grafana/home.json
      token: ''
    otel-node:
      url: https://gitlab.com/sergiojvg/home-media/-/raw/main/dashboards/grafana/Otel_Node.json
      token: ''
      #gnetId: 18682
      #revision: 3
      datasource: Prometheus
    otel-kubeletstats:
      url: https://gitlab.com/sergiojvg/home-media/-/raw/main/dashboards/grafana/Otel_kubeletstats.json
      token: ''
      #gnetId: 18681
      #revision: 3
      datasource: Prometheus
    otel-clusterview:
      url: https://gitlab.com/sergiojvg/home-media/-/raw/main/dashboards/grafana/Otel_Cluster_View.json
      token: ''
      datasource: Prometheus
    metallb:
      gnetId: 14127
      revision: 1
      datasource: Prometheus
    prom-alerts:
      gnetId: 11098
      revision: 1
      datasource: Prometheus
    blackbox1:
      gnetId: 7587
      revision: 3
      datasource: Prometheus
    blackbox2:
      gnetId: 13659
      revision: 1
      datasource: Prometheus
EOF
  ]
}

#resource "kubernetes_ingress_v1" "grafana2_external" {
#  metadata {
#    name = "grafana2-external"
#    annotations = {
#      "kubernetes.io/ingress.class"    = "nginx"
#      "cert-manager.io/cluster-issuer" = data.terraform_remote_state.infra.outputs.certmanager_clusterissuer_name
#      "nginx.org/redirect-to-https"    = true
#    }
#  }
#  spec {
#    tls {
#      secret_name = data.terraform_remote_state.infra.outputs.wildcard_ssl_certificate_secret_name
#      hosts       = ["grafana2.${data.terraform_remote_state.infra.outputs.public_domain}"]
#    }
#    rule {
#      host = "grafana2.${data.terraform_remote_state.infra.outputs.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "grafana"
#              port {
#                number = 80
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.grafana]
#}
