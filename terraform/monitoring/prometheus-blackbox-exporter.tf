#########################################################################
########################## PROMETHEUS BLACKBOX EXPORTER #####################
#########################################################################

resource "helm_release" "prometheus_blackbox_exporter" {
  name       = "prometheus-blackbox-exporter"
  chart      = "prometheus-blackbox-exporter"
  repository = "https://prometheus-community.github.io/helm-charts"
  namespace  = "default"
  timeout    = "100"
  version    = var.versions.prometheus_blackbox_exporter.chart

  values = [<<EOF
resources:
  limits:
    cpu: 300m
    memory: 60Mi
  requests:
    cpu: 250m
    memory: 60Mi
pspEnabled: false
config:
  modules:
    http_2xx:
      prober: http
      timeout: 5s
      http:
          valid_http_versions:
              - HTTP/1.1
              - HTTP/2.0
          preferred_ip_protocol: ip4
          ip_protocol_fallback: true
          follow_redirects: true
          enable_http2: true
    icmp:
      prober: icmp
      timeout: 2s
      icmp:
        preferred_ip_protocol: ip4
    google:
      prober: dns
      timeout: 5s
      dns:
        transport_protocol: "udp"
        preferred_ip_protocol: "ip4"
        query_name: "www.google.com"
        query_type: "A"
        valid_rcodes:
          - NOERROR
    ha-my-house-duckdns-org:
      prober: dns
      timeout: 5s
      dns:
        transport_protocol: "udp"
        preferred_ip_protocol: "ip4"
        query_name: "homeassistant.${data.terraform_remote_state.infra.outputs.public_domain}"
        query_type: "A"
        valid_rcodes:
          - NOERROR
    plex-my-house:
      prober: dns
      timeout: 5s
      dns:
        transport_protocol: "udp"
        preferred_ip_protocol: "ip4"
        query_name: "plex.${data.terraform_remote_state.infra.outputs.public_domain}"
        query_type: "A"
        valid_rcodes:
          - NOERROR
ingress:
  enabled: true
  annotations: 
      kubernetes.io/ingress.class: nginx
  hosts:
    - host: blackbox.my.house
      paths:
        - path: /
          pathType: ImplementationSpecific
EOF
  ]
}
