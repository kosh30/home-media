resource "helm_release" "otel_collector_deployment" {
  name       = "opentelemetry-collector-deployment"
  chart      = "opentelemetry-collector"
  repository = "https://open-telemetry.github.io/opentelemetry-helm-charts"
  namespace  = "default"
  timeout    = "60"
  version    = var.versions.otel_collector.chart

  values = [<<EOF
mode: "deployment"
livenessProbe:
  initialDelaySeconds: 10
  periodSeconds: 15
  timeoutSeconds: 5
  failureThreshold: 25
config:
  extensions:
    health_check: {}
    memory_ballast:
      size_in_percentage: 40
    basicauth/metrics:
      client_auth:
        username: "${data.grafana_cloud_stack.my_stack.prometheus_user_id}"
        password: "${grafana_cloud_api_key.prometheus.key}"
  exporters:
    logging: null
    prometheusremotewrite/grafana_cloud:
      endpoint: ${data.grafana_cloud_stack.my_stack.prometheus_remote_write_endpoint}
      auth:
        authenticator: basicauth/metrics
      resource_to_telemetry_conversion:
        enabled: true
    prometheusremotewrite/local:
      endpoint: http://prometheus-server/api/v1/write
      resource_to_telemetry_conversion:
        enabled: true
  processors:
    resource/remove_container_id:
      attributes:
        - action: delete
          key: container.id
        - action: delete
          key: container_id
    memory_limiter:
      #check_interval: 1s
      #limit_percentage: 50
      #spike_limit_percentage: 30
    batch: 
      send_batch_size: 8192
      timeout: 1s
  receivers:
    k8s_cluster:
      collection_interval: 15s
      node_conditions_to_report:
        - Ready
        - MemoryPressure
      allocatable_types_to_report:
        - cpu
        - memory
        - storage
        - ephemeral-storage
    prometheus/local:
      config:
        scrape_configs:
          - job_name: opentelemetry-collector
            scrape_interval: 10s
            static_configs:
              - targets:
                  - 0.0.0.0:8888
          - job_name: 'metallb-controller'
            kubernetes_sd_configs:
              - role: endpoints
            scheme: http
            metrics_path: '/metrics'
            relabel_configs:
              - source_labels:
                - __meta_kubernetes_service_label_name
                action: keep
                regex: metallb-controller-monitor-service
              - source_labels:
                - __meta_kubernetes_endpoint_port_name
                action: keep
                regex: metrics
          - job_name: nginx
            kubernetes_sd_configs:
                - role: pod
            relabel_configs:
              - action: keep
                regex: nginx-ingress
                source_labels:
                  - __meta_kubernetes_pod_label_app_kubernetes_io_name
              - action: keep
                regex: 9113
                source_labels:
                  - __meta_kubernetes_pod_container_port_number
          - job_name: node-problem-detector
            kubernetes_sd_configs:
                - role: pod
            metrics_path: /metrics
            relabel_configs:
              - action: keep
                regex: node-problem-detector
                source_labels:
                  - __meta_kubernetes_pod_label_app_kubernetes_io_name
              - action: keep
                regex: exporter
                source_labels:
                  - __meta_kubernetes_pod_container_port_name
          - job_name: 'prometheus-blackbox-exporter-http'
            metrics_path: /probe
            params:
              module: [http_2xx]
            static_configs:
              - targets:
                - https://google.com
                - https://homeassistant.${data.terraform_remote_state.infra.outputs.public_domain}
                - https://overseerr.${data.terraform_remote_state.infra.outputs.public_domain}/api/v1/status
                - http://homeassistant-home-assistant:8123
                - http://overseerr:5055/api/v1/status
            relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: prometheus-blackbox-exporter:9115
          - job_name: 'prometheus-blackbox-exporter-google-dns'
            metrics_path: /probe
            params:
              module: [google]
            static_configs:
              - targets:
                - 192.168.1.220
            relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: prometheus-blackbox-exporter:9115
          - job_name: 'prometheus-blackbox-exporter-icmp'
            metrics_path: /probe
            params:
              module: [icmp]
            static_configs:
              - targets:
                - 192.168.1.1
                - 192.168.1.201
                - 192.168.1.202
                - 192.168.1.203
                - 192.168.1.204
                - 192.168.1.205
            relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: prometheus-blackbox-exporter:9115
    prometheus/grafana_cloud:
      config:
        scrape_configs:
          - job_name: 'homeassistant'
            kubernetes_sd_configs:
              - role: endpoints
            scheme: http
            metrics_path: '/api/prometheus'
            # Long-Lived Access Token
            bearer_token: '${data.sops_file.secrets.data["homeassistant_bearer_token"]}'
            #metric_relabel_configs:
            #  - action: drop
            #    source_labels: [__name__]
            #    regex: homeassistant_(state|entity|last)_.*
            relabel_configs:
              - source_labels:
                - __meta_kubernetes_service_label_app_kubernetes_io_instance
                action: keep
                regex: homeassistant
              - source_labels:
                - __meta_kubernetes_service_label_app_kubernetes_io_name
                action: keep
                regex: home-assistant
              - source_labels:
                - __meta_kubernetes_endpoint_port_name
                action: keep
                regex: http
  service:
    telemetry:
      metrics:
        address: 0.0.0.0:8888
      #logs:
      #  level: "debug"
    extensions:
      - health_check
      - memory_ballast
      - basicauth/metrics
    pipelines:
      metrics/local:
        exporters:
          - prometheusremotewrite/local
        processors:
          - memory_limiter
          - batch
          - resource/remove_container_id
        receivers:
          - k8s_cluster
          - prometheus/local
          - prometheus/grafana_cloud
      metrics/grafana_cloud:
        exporters:
          - prometheusremotewrite/grafana_cloud
        processors:
          - memory_limiter
          - batch
          - resource/remove_container_id
        receivers:
          - prometheus/grafana_cloud
      traces: null
      logs: null
resources:
  requests:
    cpu: 200m
    memory: 600Mi
  limits:
    cpu: 300m
    memory: 700Mi
clusterRole:
  create: true
  rules: 
  - apiGroups:
    - ''
    - 'apps'
    - 'extensions'
    - 'batch'
    - 'autoscaling'
    resources:
    - '*'
    verbs:
    - 'get'
    - 'list'
    - 'watch'
  - nonResourceURLs:
    - '/metrics'
    verbs:
    - 'get'
EOF
  ]
}

resource "helm_release" "otel_collector_daemonset" {
  name       = "opentelemetry-collector-daemonset"
  chart      = "opentelemetry-collector"
  repository = "https://open-telemetry.github.io/opentelemetry-helm-charts"
  namespace  = "default"
  timeout    = "60"
  version    = var.versions.otel_collector.chart

  values = [<<EOF
mode: daemonset
livenessProbe:
  initialDelaySeconds: 24
  periodSeconds: 15
  timeoutSeconds: 5
  failureThreshold: 15
extraEnvs:
  - name: OTEL_SERVICE_NAME
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: "metadata.labels['app.kubernetes.io/component']"
  - name: OTEL_K8S_NAMESPACE
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: metadata.namespace
  - name: KUBE_NODE_NAME
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: spec.nodeName
  - name: OTEL_K8S_NODE_NAME
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: spec.nodeName
  - name: OTEL_K8S_POD_NAME
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: metadata.name
  - name: OTEL_K8S_POD_UID
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: metadata.uid
  - name : OTEL_RESOURCE_ATTRIBUTES
    value: service.name=$(OTEL_SERVICE_NAME),service.instance.id=$(OTEL_K8S_NODE_NAME),service.namespace=$(OTEL_K8S_NAMESPACE),k8s.namespace.name=$(OTEL_K8S_NAMESPACE),k8s.node.name=$(OTEL_K8S_NODE_NAME),k8s.cluster.name=home
presets:
  logsCollection:
    enabled: true
    includeCollectorLogs: false
    storeCheckpoints: true
  hostMetrics:
    enabled: true
config:
  extensions:
    health_check: {}
    memory_ballast:
      size_in_percentage: 40
    basicauth/logs:
      client_auth:
        username: "${data.grafana_cloud_stack.my_stack.logs_user_id}"
        password: "${grafana_cloud_api_key.loki.key}" 
  exporters:
    logging: null
    loki:
      endpoint: "https://${data.grafana_cloud_stack.my_stack.logs_user_id}:${grafana_cloud_api_key.loki.key}@${trimprefix(data.grafana_cloud_stack.my_stack.logs_url, "https://")}/api/prom/push"
      auth:
        authenticator: basicauth/logs
    prometheusremotewrite/local:
      endpoint: http://prometheus-server/api/v1/write
      resource_to_telemetry_conversion:
        enabled: true
  processors:
    resourcedetection/env:
      detectors: [env]
    resource/remove_pod_name:
      attributes:
        - action: delete
          key: k8s.pod.name
        - action: delete
          key: k8s_pod_name
    resource/logs:
      attributes:
        - action: insert 
          key: log_file_path
          from_attribute: log.file.path
        - action: insert 
          key: log_iostream
          from_attribute: log.iostream
        - action: insert 
          key: k8s_pod_name
          from_attribute: k8s.pod.name
        - action: insert 
          key: k8s_container_name
          from_attribute: k8s.container.name
        - action: insert 
          key: k8s_container_restart_count
          from_attribute: k8s.container.restart_count
        - action: insert 
          key: k8s_namespace_name
          from_attribute: k8s.namespace.name
        - action: insert
          key: loki.resource.labels
          value: k8s_pod_name, k8s_container_name, k8s_container_restart_count, k8s_namespace_name, log_file_path, log)iostream 
    batch: 
      send_batch_size: 8192
      timeout: 1s
  receivers:
    kubeletstats:
      collection_interval: 20s
      auth_type: "serviceAccount"
      endpoint: "https://$${env:OTEL_K8S_NODE_NAME}:10250"
      insecure_skip_verify: true
      extra_metadata_labels:
        - k8s.volume.type
      metric_groups:
        - container
        - pod
        - volume
        - node
    hostmetrics:
      collection_interval: 10s
      root_path: /hostfs
      scrapers:
        cpu: 
          metrics:
            system.cpu.utilization:
              enabled: true
        disk: null
        filesystem:
          metrics:
            system.filesystem.utilization:
              enabled: true
          exclude_fs_types:
            fs_types:
            - autofs
            - binfmt_misc
            - bpf
            - cgroup2
            - configfs
            - debugfs
            - devpts
            - devtmpfs
            - fusectl
            - hugetlbfs
            - iso9660
            - mqueue
            - nsfs
            - overlay
            - proc
            - procfs
            - pstore
            - rpc_pipefs
            - securityfs
            - selinuxfs
            - squashfs
            - sysfs
            - tracefs
            match_type: strict
          exclude_mount_points:
            match_type: regexp
            mount_points:
            - /dev/*
            - /proc/*
            - /sys/*
            - /run/k3s/containerd/*
            - /var/lib/docker/*
            - /var/lib/kubelet/*
            - /snap/*
        load: null
        memory: 
          metrics:
            system.memory.utilization:
              enabled: true
        network: null
  service:
    #telemetry:
    #  logs:
    #    level: "debug"
    extensions:
      - health_check
      - memory_ballast
      - basicauth/logs
    pipelines:
      traces: null
      metrics/hostmetrics:
        exporters:
          - prometheusremotewrite/local
        processors:
          - memory_limiter
          - batch
          - resourcedetection/env
          - resource/remove_pod_name
        receivers:
          - hostmetrics
      metrics/kubeletstats:
        exporters:
          - prometheusremotewrite/local
        processors:
          - memory_limiter
          - batch
        receivers:
          - kubeletstats
      logs:
        exporters:
          - loki
        processors:
          - memory_limiter
          - batch
          - resource/logs
        receivers:
          - filelog
resources:
  requests:
    cpu: 150m
    memory: 450Mi
  limits:
    cpu: 250m
    memory: 600Mi
clusterRole:
  create: true
  rules: 
  - apiGroups:
    - ''
    resources:
    - '*'
    verbs:
    - 'get'
    - 'list'
    - 'watch'
  - nonResourceURLs:
    - '/metrics'
    verbs:
    - 'get'
EOF
  ]
}
