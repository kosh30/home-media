# Overview

This is a full setup for a kubernetes home cluster. This setup is fully automated (from top to bottom) using Ansible and Terraform. First, it will use ansible to configure X amount of nodes running (proably) any Debian based distribution. Then, it will install a Kubernetes distribution and create a cluster (using [k3s](https://docs.k3s.io/)). Then, it will create a SAN (using [longhornSDS](https://longhorn.io/) plus another storage system via NFS. A ton of services are also installed and configured. This is an overview of what it does.

* Bootstrap and prepare basic Debian based servers (more information under Tested Platforms)
* Optionally create an NFS server
* Install Kubernetes and configure a cluster
* Install and configure Longhorn SDS via [this](terraform/infra) module
* Install and configure CertManager clusterissuer
* Set up a bunch of other infra related services like DNS and Homeassistant (more info [here](terraform/infra/))
* Set up services for multimedia applications (torrents, trackers, cloud...). More information [here](terraform/media-services/)
* Set up a pre-configured (and a bit extensive) monitoring stack using promtail, prometheus, grafana, grafana cloud and others. More info [here](terraform/monitoring/)
* ~ 20 docker containers
* ~ 5 external free Cloud services

First, a quick overview of the architecture is explained, followed by the ansible config to deploy the k3s cluster and ending with the terraform setup for the services.

## Architecture

Currently, this setup will (should) work in any cluster with one or more compute nodes (in my case, I use 4 SBC from different manufacturers for my cluster). The first node will contain the kubernetes server and will also act as a compute node (worker). Regarding storage, two different systems are used:
* Longhorn SDS: This is installed in the same compute nodes and uses the space in the root partition, nothing else is needed. Because of this, this setup can also be considered a hyperconverged infrastructure. This can also be modified to use different disks. The reason why Longhorn is needed is mostly as a fast storage that provides a Kubernetes Storage Class. NFS is not a good fit for this.
* NFS: As a bulky slow storage I use a NFS server. Previously I used to have an external USB drive connected to one of the SBCs (servers), but a USB drive connected to a USB port of an SBC is a nightmare, because of this, I took the lazy route and just bougbht a cheap qnap appliance to be only used as a NFS server. I still keep the ansible code to deploy your own NFS server without the need of an external appliance, this is explained below. This storage is great to cheaply store a bunch of data (like movies).

## Tested platforms

These are the SBCs I have used so far. In theory, any SBC using ARM64 architecture should work and it is also likely that other architectures also work, I just have not tested it. I highly recommend [ARMbian](https://www.armbian.com/) for the SBCs. It is an amazing project and it allows some sort of homogeneity acros all the servers (specially useful when you have a bunch of different SBCs). Using the same acrchitecture is also recommended for simplicity.

<!-- markdown-link-check-disable -->

| SBC       |  OS                                                                   | Arch  | Notes                                |
|-----------|-----------------------------------------------------------------------|-------|--------------------------------------|
| Pine64    | [Ubuntu 18.04 (Armbian)](https://www.armbian.com/pine64/)             | ARM64 |                                      |
| Pine64    | [Ubuntu 20.04 (Armbian)](https://www.armbian.com/pine64/)             | ARM64 |                                      |
| Pine64    | [Debian Buster(Armbian)](https://www.armbian.com/pine64/)             | ARM64 |                                      |
| Pine64    | [Ubuntu 22.04 (Armbian self built)](https://github.com/armbian/build) | ARM64 |                                      |
| Rock64    | [Ubuntu 18.04 (Ayufan)](https://wiki.pine64.org/index.php/ROCK64_Software_Release#Ubuntu_18.04_Bionic_minimal_64bit_.28arm64.29_Image_.5BmicroSD_.2F_eMMC_Boot.5D_.5B0.9.14.5D)  | ARM64 |                                      |
| Rock64    | [Ubuntu 20.04 (Armbian)](https://www.armbian.com/rock64/)             | ARM64 | [MAC Bug](./docs/RK3328_MAC_issue.md)|
| Rock64    | [Debian Buster (Armbian)](https://www.armbian.com/rock64/)            | ARM64 | [MAC Bug](./docs/RK3328_MAC_issue.md)|
| Rock64    | [Ubuntu 22.04 (Armbian self built)](https://github.com/armbian/build) | ARM64 |                                      |
| Rockpi-4c | [Ubuntu 22.04 (Armbian self built)](https://github.com/armbian/build) | ARM64 |                                      |

<!-- markdown-link-check-enable -->

# Installing kubernetes (Ansible setup)

## Dependencies

The following steps are required before running or configuring Ansible

#### Client

In Ansible, client is whetever you are going to run ansible from. Most likely your PC. Ansible is required, it can be found in your package manager

```
pacman -S ansible
apt-get install ansible
.....or something like that
```

Then, clone this repository and it's submodules:

```
git clone --recurse-submodules git@github.com:sergiojvg/home-media.git
```

#### Servers

* Install Python (python3 and python-is-python3 for ubuntu22.04)
* Passwordless ssh access to the server's root account (set up your public key inside **/root/.ssh/authorized_keys**)

## Configure Ansible

All of your cluster settings are defined inside [*ansible/inventory*](https://gitlab.com/sergiojvg/home-media/-/blob/main/ansible/inventory). Fill out those variables depending on your infra. Hopefully the comments are enough :)

## Deploy

Test everything:

```
ansible -i inventory all -u root -m ping
```

And run:

```
ansible-playbook -i inventory infra_playbook.yaml
```

### Get new kubectl config file

Now the new k3s cluster is up. Copy the k3s client file (already fetched and configured by ansible) into the root directory (terraform kubernetes providers use this location):

```
cp fetched_files/$NODE_IP/etc/rancher/k3s/k3s.yaml ../k3s.yaml
```

Replace **$NODE_IP** for the IP of the master k3s node.

# Terraform setup

The terraform setup is divided into three root modules. Reason for this is that a single root module grew so long that a simple *terraform plan* was taking more than 5 minutes. First, the infra root module should be executed, followed by media services and lastly monitoring. Before running any root module, some dependencies are required.

## Terraform dependencies

### Terraform

Install terraform on your computer (duh)

### Gitlab AT

Gitlab is used as a backend to store the terraform state files (because....why not). To make this work, first [generate a Gitlab's Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). Then, store this token in an env var called: *TF_HTTP_PASSWORD*

Also set an env var called *TF_HTTP_USERNAME* containing your gitlab user name.

Lastly, modify the gitlab project_id inside *providers.tf* on each root module:

```
terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/monitoring"
    lock_address   = "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/monitoring/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/monitoring/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
```

### Age output file
Sensitive information is encrypted using [Mozilla's SOPS](https://github.com/mozilla/sops) and [age](https://github.com/mozilla/sops#encrypting-using-age).

Install [age](https://github.com/FiloSottile/age).

And generate your key file:

```
age-keygen -o $HOME/.config/sops/age/keys.txt
```

Copy your public key, it will be used later on.


### Optional: Gitlab CICD pipeline

This step can be skipped, it is only a requirement for setting up a Gitlab CICD pipeline.

Create a variable called *age_key*, type *File*, with the content of the age decrypt key

## Running terraform

All the common dependencies are already set. Modules should be executed in order.

### Terraform Infra module

This is the first root module that must be installed. Follow the instructions [here](terraform/infra).

### Terraform Media Services module

This module should be installed after. Follow the instructions [here](terraform/home-services).

### Terraform Monitoring module

This module is kind of optional. It will set up a biggish monitoring stack for your cluster. Follow the instructions [here](terraform/monitoring).

## Access services

The full list of services available and their URLs can be seen by running:

```
kubectl -n deefault get ingress
```


## Restore backups

### HA

```
git init
git remote add origin https://github.com/sergiojvg/homeassistant.git
git clean -fd
git pull origin master
```
