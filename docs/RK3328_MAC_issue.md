# Rockchip RK3328 MAC issue

Rock64 boards will always use same MAC in a modern kernel (ex: Ubuntu Focal or Debian Buster).

The problem seems to be that they have enabled upstream that the mac-adress should be derived from the CPUID, but there is no driver to extract the CPUID from the efuse on rk3328, only rk3399.

To validate, run:

```
xxd /sys/bus/nvmem/devices/rockchip-efuse0/nvmem
```

and something like this should show up:

```
00000000: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000010: 0000 0000 0000 0000 0000 0000 0000 0000  ................
```

# Fix

Idea here is to manually set the MAC address (or randomize it).

## Using NetworkManager

Recommended.

```
cat << EOF > /etc/NetworkManager/conf.d/ethernet_random_mac.conf
[connection-mac-randomization]
ethernet.cloned-mac-address=stable
EOF
```

## Using systemd

[spoof the MAC address](https://wiki.archlinux.org/index.php/MAC_address_spoofing#systemd-networkd)
