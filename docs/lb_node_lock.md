# Lock Load Balancer

By default, load balancer can use any node's IP to listen. This means that the DNS and ingres IP might change. To prevent this, only run LB in master node (if that node goes down...everyhing fails anyway).

Add the following label to the master node:

```
svccontroller.k3s.cattle.io/enablelb: "true"
```

I need to automate this. Adding the flag *--node-label* to k3s master's installation parameters as described [here](https://rancher.com/docs/k3s/latest/en/installation/install-options/server-config/) might be the solution.
