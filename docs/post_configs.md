# Post Config


After everything is up, a few configurations are recommended. These configurations are done in the default backup available with this repo, but if you want to do it manually, this is a good starting point.

## Zerotier

Accept your device if you need to.

Under **DNS** set your domain and DNs server (adguard LB Service IP).

Add a route to your internal network via the IP assigned to k8s master node.

## Media

This is for the 25 apps required to download freakin movies.

### Qbittorrent

The idea here is that Movies added by Radarr are downloaded to a folder, while series downloaded by Sonarr go to a different folder. Because of this Plex can be configured later with two different libraries (Movies and Series) pointing to different folders.

Before bittorrent, manually create the required folders. Connect to the master node (where the NFS mount is), and create two folders: **radarr** and **sonarr** inside the **nfs-pvc** pvc (located in *$NFS_EXPORTED_DIR/default-nfs-pvc-SOMETHING*).

#### Torrent Management mode

<!-- markdown-link-check-disable-next-line -->
Go to http://torrent.my.house, log in (admin -- adminadmin). Go to **Tools** --> **Options** --> **Downloads** --> **Saving Management** and set **Default Torrent Management Mode** to **Automatic**.

#### Remove credentials

Go to **Tools** --> **Web UI** --> **Bypass authentication for clients in whitelisted IP subnets** and configure the following address range:

```
10.0.0.0/8
```

k3s uses 10.42.0.0/16 for pod network and 10.43.0.0/16 for service network, so all requests will arrive from those IPs.

### Jacket

Log in to jackett and add your trackers. These are recommended:

For Movies:
* YTS

For TV Shows:
* LimeTorrents
* MejorTorrent
* The Pirate Bay
* Torrent Downloads

### Radarr and Sonarr

#### Root Folder

Create Root folder. Go to **Settings** --> **Media Management** --> **Root Folders** --> **Add Root Folder**

Set anything inside downloads. For example: */downloads/radarr*.

#### Indexer

Configure jacket as a tracker. Go to **Settings** --> **Indexer** --> **Add** --> **Torznab** --> **Custom**

* Enable RSS Sync:  Yes
* Enable Search:  Yes
<!-- markdown-link-check-disable-next-line -->
* URL: http://jackett:9117/api/v2.0/indexers/all/results/torznab
* API Key:  *copy/paste from Jackett* 
* Categories:  5000,5030,5040
* Minimum Seeders:  5

#### Torrent

Configure qbittorrent as a download client. Go to **Settings** --> **Download Clients** --> **Add** --> **Qbittorrent**

* Host: qbittorrent
* Port: 8080

If you remove auth for k3s pod network as specified before, this is it. If not, add user and pass.

* Username: *admin*
* Password:  adminadmin

### Ombi

#### Remove user and pass

Go to **Settings** --> **Configuration** --> **Authentication** --> Allow users to login without a password

#### Movies config

Configure Radarr. Go to **Settings** --> **Movies** --> **Radarr**

* Enable: True
* Hostname: radarr
* Port: 7878
* Api Key: Go to Radarr --> Settings --> General --> Copy API Key
* Set default minimum availability

#### TV Shows config

Configure Radarr. Go to **Settings** --> **TV** --> **Sonarr**

* Enable: True
* Hostname: sonarr
* Port: 8989
* Api Key: Go to Sonarr --> Settings --> General --> Copy API Key

#### Media Server

This allows Ombi to detect whena a request is available.

Set your user and password on PLex Credentials and click Load Servers. This is the easiest way to get Plex Authorization Token and Machine Identifier. After server is loaded, replace these elements:

* Hostname or IP: **plex-tcp**
* Port: **32400**

### Plex

#### Media Libraries

Plex will read content from the folders **radarr** (for Movies) and **sonarr** for Series. Both of this folders are located in the root folder of **nfs-pvc**. Both Radarr and Sonarr will automatically add their respective content into these folders.

Go to **Settings** --> **Manage** --> **Libraries** --> **Add Library**
